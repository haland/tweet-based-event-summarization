clear all; clc;

thrift_starts_with = [32, 31, 30. 31, 33, 34, 36];

thrift_topic_first_binaryprefix = [35, 142, 434, 576, 678, 679, 688];
thrift_document_first_regexstring = [42, 35, 34, 35, 146, 51, 38];

java_topic_first_binaryprefix = [6, 7, 6, 6, 6, 7, 6];
java_document_first_regexstring = [6, 7, 6, 6, 6, 7, 6];

% Plot follows:

shows = 1:length(thrift_starts_with);
plots = [thrift_starts_with; thrift_topic_first_binaryprefix;...
    thrift_document_first_regexstring; java_topic_first_binaryprefix;...
    java_document_first_regexstring];

plot(shows, plots);
xlabel('Show');
ylabel('Response time (ms)');

grid on;
legend('Thrift startsWith()', 'Thrift binary prefix',...
    'Thrift regex string', 'Java binary prefix', 'Java regex string');
function plot_precision_with_feedback( i, name, xmin, rf, nb, id, in,...
    pos, neg )

    figure(i);
    
    classifiers = [rf; nb; id];
    feedback = (pos-neg);
    days = 1:length(in);
    
    [haxes, h1, h2] = plotyy(days, classifiers, days,...
        feedback, 'plot', 'stem');

    grid on;
    set(h2, 'LineStyle', 'none');

    ylim(haxes(1), [xmin 1]);
    xlim(haxes(1), [1 length(days)]);
    xlim(haxes(2), [1 length(days)]);

    ylabel(haxes(1), 'Precision');
    ylabel(haxes(2), 'Feedback');
    xlabel(haxes(1), 'Training model (chronological)');

    title(name);
    legend(haxes(1), 'Random Forrest', 'Naive Bayes', 'C4.5');
end


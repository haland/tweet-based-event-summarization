function plot_precision( i, name, xmin, rf, nb, id )
    figure(i);
    
    classifiers = [rf; nb; id];
    days = 1:length(rf);
    
    plot(days, classifiers);

    grid on;

    ylim([xmin 1]);
    xlim([1 length(days)]);

    ylabel('Precision');
    xlabel('Training model (chronological)');

    title(name);
    legend('Random Forrest', 'Naive Bayes', 'C4.5');
end


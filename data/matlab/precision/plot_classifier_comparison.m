function plot_classifier_comparison( i, name, days, xmin, single, split,...
    individual )
    
    figure(i);
  
    plots = [single; split; individual];
    
    plot(days, plots);
    
    grid on;
    
    ylim([xmin 1]);
    xlim([1 length(days)]);

    ylabel('Precision');
    xlabel('Training model (chronological)');

    title(name);
    legend('Single training model', 'Split training models',...
        'Individual training models');
end


clear all; clc;
micro_data_split();

ambiguous = [rfAmbiguous; nbAmbiguous; idAmbiguous; inAmbiguous;...
    poAmbiguous; neAmbiguous];
unambiguous = [rfUnambiguous; nbUnambiguous; idUnambiguous; inUnambiguous;...
    poUnambiguous; neUnambiguous];

topics = [ambiguous; unambiguous];

xmin = [0.4, 0.9];
names = {'Ambiguous', 'Unambiguous'};

for i = 1:length(names)
	plot_precision_with_feedback(i, names(i), xmin(i), topics(i*6-5,:),...
        topics(i*6-4,:), topics(i*6-3,:), topics(i*6-2,:),...
        topics(i*6-1,:), topics(i*6,:));
end
clear all; clc;
data_individual();

agents = [rfAgents; nbAgents; idAgents; inAgents; poAgents; neAgents];

bigBang = [rfBigBang; nbBigBang; idBigBang; inBigBang; poBigBang;...
    neBigBang];

doctorWho = [rfDoctorWho; nbDoctorWho; idDoctorWho; inDoctorWho;...
    poDoctorWho; neDoctorWho];

firefly = [rfFirefly; nbFirefly; idFirefly; inFirefly; poFirefly;...
    neFirefly];

hannibal = [rfHannibal; nbHannibal; idHannibal; inHannibal; poHannibal;...
    neHannibal];

suits = [rfSuits; nbSuits; idSuits; inSuits; poSuits; neSuits];

supernatural = [rfSupernatural; nbSupernatural; idSupernatural;...
    inSupernatural; poSupernatural; neSupernatural];

vikings = [rfVikings; nbVikings; idVikings; inVikings; poVikings;...
    neVikings];

whoseLine = [rfWhoseLine; nbWhoseLine; idWhoseLine; inWhoseLine;...
    poWhoseLine; neWhoseLine];

xmin = [0.9, 0.9, 0.9, 0.4, 0.9, 0.4, 0.4, 0.4, 0.9];

topics = [agents; bigBang; doctorWho; firefly; hannibal; suits;...
    supernatural; vikings; whoseLine];

names = {'Agents of S.H.I.E.L.D.', 'The Big Bang Theory', 'Doctor Who',...
   'Firefly', 'Hannibal', 'Suits', 'Supernatural', 'Vikings',...
   'Whose Line Is It Anyway?'};

for i = 1:length(names)
	plot_precision_with_feedback(i, names(i), xmin(i), topics(i*6-5,:),...
        topics(i*6-4,:), topics(i*6-3,:), topics(i*6-2,:),...
        topics(i*6-1,:), topics(i*6,:));
end
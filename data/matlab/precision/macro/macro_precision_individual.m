clear all; clc;
data_individual();

rf = (rfAgents + rfBigBang + rfDoctorWho + rfFirefly + rfHannibal + ...
    rfSuits + rfSupernatural + rfWhoseLine)./9;
nb = (nbAgents + nbBigBang + nbDoctorWho + nbFirefly + nbHannibal + ...
    nbSuits + nbSupernatural + nbWhoseLine)./9;
id = (idAgents + idBigBang + idDoctorWho + idFirefly + idHannibal + ...
    idSuits + idSupernatural + idWhoseLine)./9;

plot_precision(1, '', 0.7, rf, nb, id);
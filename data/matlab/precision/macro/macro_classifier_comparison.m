clear all; clc;

macro_data_single();
macro_data_split();
data_individual();

days = 1:length(rf);

rfSingle = rf;
rfSplit = (rfAmbiguous+rfUnambiguous)./2;
rfIndividual = (rfAgents+rfBigBang+rfDoctorWho+rfFirefly+rfHannibal+...
    rfSuits+rfSupernatural+rfVikings+rfWhoseLine)./9;

plot_classifier_comparison(1, 'Random Forest', days, 0.8, rfSingle,...
    rfSplit, rfIndividual);

nbSingle = nb;
nbSplit = (nbAmbiguous+nbUnambiguous)./2;
nbIndividual = (nbAgents+nbBigBang+nbDoctorWho+nbFirefly+nbHannibal+...
    nbSuits+nbSupernatural+nbVikings+nbWhoseLine)./9;

plot_classifier_comparison(2, 'Naive Bayes', days, 0.8, nbSingle,...
    nbSplit, nbIndividual);

idSingle = id;
idSplit = (idAmbiguous+idUnambiguous)./2;
idIndividual = (idAgents+idBigBang+idDoctorWho+idFirefly+idHannibal+...
    idSuits+idSupernatural+idVikings+idWhoseLine)./9;

plot_classifier_comparison(3, 'C4.5', days, 0.8, idSingle,...
    idSplit, idIndividual);
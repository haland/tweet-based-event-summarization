clear all; clc;
macro_data_split();

plot_precision(1, 'Ambiguous', 0.4, rfAmbiguous, nbAmbiguous,...
    idAmbiguous);
plot_precision(2, 'Unambiguous', 0.9, rfUnambiguous, nbUnambiguous,...
    idUnambiguous);

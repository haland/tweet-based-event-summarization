clear all; clc;

% Number of documents passed through the system
d = [325324, 299768, 203979, 308829, 280006, 284758, 244990, 299303,...
    323944, 369937, 339628, 329481, 294748, 277334, 294704, 357646,...
    373258, 339466, 304894, 310749, 289265, 318854, 328500, 414897,...
    336080, 300669, 342567, 321623, 324333, 381645, 390681, 336967,...
    373250, 364694, 288292, 442435, 367955, 422903, 382710, 359344];

% Number of documents passed first filtering step
d1 = [32828, 41514, 41734, 37162, 42395, 53913, 32878, 27601, 30611,...
    87641, 35805, 36604, 60320, 32313, 27907, 29828, 62511, 35417,...
    36753, 51169, 28895, 28331, 30651, 68671, 33166, 36890, 60372,...
    32076, 27948, 28024, 63961, 32543, 81606, 73129, 38337, 28217,...
    29925, 83121, 40235, 40317];

% Number of documents passed second filtering step
d2 = [0, 0, 40845, 35403, 13802, 18301, 8304, 6649, 6960, 26311, 8621,...
    9853, 17283, 7014, 5456, 5942, 19536, 8551, 11484, 15102, 6318,...
    6212, 6553, 23189, 8562, 10291, 21469, 7635, 6196, 5787, 21856,...
    7574, 13306, 21016, 7366, 5421, 6432, 26588, 10004, 9754];

% Model Instance Count
c = [0, 0, 737, 737, 1261, 1386, 1386, 1606, 1610, 1737, 1737, 1737,...
    1976, 1976, 1976, 1976, 1976, 2047, 2148, 2148, 2148, 2148, 2238,...
    2319, 2319, 2319, 2475, 2475, 2475, 2501, 2593, 2593, 2737, 2863,...
    2989, 2989, 3031, 3031, 3031, 3108];

days = 1:(length(c));

% Documents passed filtering steps, day by day
figure(1);

filters = [d1; d2];
[haxes, hline1, hline2] = plotyy(days, filters, days, d, 'plot',...
    'semilogy');

grid on;
xlabel('Days');
xlim(haxes(1), [1 length(days)]);
xlim(haxes(2), [1 length(days)]);
ylabel(haxes(1), 'Number of documents that passed the filter');
ylabel(haxes(2), 'Total documents passed through the system');
legend(haxes(1), 'First filtering step', 'Second filtering step');

% Percent of documents passed both filters compared to feedback count, 
% day by day
figure(2);

percent = [d1(1:2), d2(3:length(d2))]./d;
[haxes, hline1, hline2] = plotyy(days, percent, days, c, 'plot', 'stem');

grid on;
xlabel('Days');
xlim(haxes(1), [1 length(days)]);
xlim(haxes(2), [1 length(days)]);
ylabel(haxes(1), 'Percent of documents passed filter');
ylabel(haxes(2), 'Number of feedback instances');
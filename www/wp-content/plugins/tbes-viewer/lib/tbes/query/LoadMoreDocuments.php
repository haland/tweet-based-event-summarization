<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../../../../../../wp-load.php');
require_once('../handlers/DocumentHandler.php');
require_once('../../thrift/ThriftLoader.php');

$settings = get_option('tbes_viewer_settings', false);

if (!$settings || count($settings) < 3 || count($settings['thrift']) < 2) {
    echo 'Settings not properly configured for tBES Viewer.';
} else {
    $thrift_settings = $settings['thrift'];
    $hbase_connection = new HbaseConnection($thrift_settings['host'], $thrift_settings['port']);

    $mysql_settings = $settings['mysql'];
    $mysql_connection = @mysqli_connect($mysql_settings['host'], $mysql_settings['user'], $mysql_settings['password'],
        $mysql_settings['database']);

    $topic_handler = new TopicHandler($mysql_connection);
    $document_handler = new DocumentHandler($hbase_connection, $settings['document_per_topic_limit']);

    $topic_id = trim($_GET['topic_id']);
    $startRow = trim($_GET['relation_id']);
    $container_id = trim($_GET['container_id']);

    $document_handler->addDocumentsForTopicFromRow($startRow, $topic_id, $container_id);
}

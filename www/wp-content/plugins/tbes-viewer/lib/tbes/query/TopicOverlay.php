<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../../../../../../wp-load.php');
require_once('../../thrift/ThriftLoader.php');
require_once('../handlers/TopicHandler.php');
require_once('../handlers/DocumentHandler.php');

function overlayHeader() { ?>
    <div class="overlay_container">
        <div id="page" class="hfeed site">
            <div id="main" class="site-main">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">
                        <div class="widget-area">
                            <aside class="widget widget_tbes_viewer" style="text-align:left" id="overlay_widget">
                                <span id="tbes_topics" class="tbes"> <?php
}

function overlayFooter() { ?>
                                </span>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <?php
}

$settings = get_option('tbes_viewer_settings', false);

if (!$settings || count($settings) < 3 || count($settings['thrift']) < 2 || count($settings['mysql']) < 3) {
    echo 'Settings not properly configured for tBES Viewer.';
} else {
    $thrift_settings = $settings['thrift'];
    $hbase_connection = new HbaseConnection($thrift_settings['host'], $thrift_settings['port']);

    $mysql_settings = $settings['mysql'];
    $mysql_connection = @mysqli_connect($mysql_settings['host'], $mysql_settings['user'],
        $mysql_settings['password'], $mysql_settings['database']);

    $topic_handler = new TopicHandler($mysql_connection);
    $document_handler = new DocumentHandler($hbase_connection, $settings['document_per_topic_limit']);

    $root_topic_id = trim($_GET['topic_id']);
    $root = $topic_handler->getTopic($root_topic_id);

    overlayHeader(); ?>

    <h2  style="background-color: rgba(241, 239, 226, 1); padding:16px 0 16px 16px"> <?php
        echo $root['name']; ?>
    </h2>

    <div class="overlay_topic" id="topic_1">
        <h3 class="tbes_topic_title">All Tweets</h3> <?php

        $containerId = 'overlay_topic_1'; ?>

        <div class="overlay_documents" id="<?php echo $containerId; ?>"> <?php
            $document_handler->addDocumentsForTopic($root_topic_id, $containerId); ?>
        </div>
    </div> <?php

    $depth = $topic_handler->treeDepth($root);
    $selected_option_id = $root_topic_id;
    $begin_at = 0;

    for($i = 2; $i <= $depth; $i++) {
        $containerId = 'overlay_topic_' . $i;
        $nodesOnLevel = $topic_handler->getTopicsOnLevel($root, $i); ?>

        <div class="overlay_topic" id="<?php echo 'topic_' . $i ?>">
			<span class="custom-dropdown custom-dropdown--white">
				<select id="<?php echo 'selection_' . $i;?>"
                        class="custom-dropdown__select custom-dropdown__select--white"
                        onchange="onSelectionChanged('<?php echo $root_topic_id;?>', '<?php echo $i;?>')"> <?php
                    for($j = 0; $j < count($nodesOnLevel); $j++) {
                        $node = $nodesOnLevel[$j];
                        if ($node['parent_id'] == $selected_option_id) { ?>
                            <option id="<?php echo $node['parent_id']; ?>" value="<?php echo $node['id']; ?>">
                                <?php echo $node['name']; ?>
                            </option> <?php
                        }
                    } ?>
                </select>
            </span>
            <div id="<?php echo $containerId;?>" class="overlay_documents" style="margin-top:10px"> <?php
                $selectedNode = $nodesOnLevel[0];
                $document_handler->addDocumentsForTopic($selectedNode['id'], $containerId); ?>
            </div>
        </div> <?php

        $selected_option_id = $selectedNode['id'];
    }

    overlayFooter();
}
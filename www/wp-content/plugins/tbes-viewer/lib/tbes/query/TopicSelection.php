<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../../../../../../wp-load.php');
require_once('../handlers/TopicHandler.php');

$settings = get_option('tbes_viewer_settings', false);

if (!$settings || count($settings) < 3 || count($settings['mysql']) < 4) {
    echo 'Settings not properly configured for tBES Viewer.';
} else {
    $level = trim((string)(intval(($_GET['level']), 10)));
    $root_topic_id = trim($_GET['root_topic_id']);
    $parent_topic_id = trim($_GET['parent_topic_id']);

    $mysql_settings = $settings['mysql'];
    $mysql_connection = @mysqli_connect($mysql_settings['host'], $mysql_settings['user'],
        $mysql_settings['password'], $mysql_settings['database']);

    $topic_handler = new TopicHandler($mysql_connection);

    $root = $topic_handler->getTopic($root_topic_id);
    $selectionTopics = $topic_handler->getTopicsOnLevel($root, $level);

    for($i = 0; $i < count($selectionTopics); $i++) {
        $topic = $selectionTopics[$i];

        if ($topic['parent_id'] == $parent_topic_id) { ?>
            <option id="<?php echo $topic['parent_id']; ?>" value="<?php echo $topic['id']; ?>">
                <?php echo $topic['name']; ?>
            </option> <?php
        }
    }
}

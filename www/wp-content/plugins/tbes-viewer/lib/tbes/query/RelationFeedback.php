<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../../../../../../wp-load.php');
require_once('../../thrift/ThriftLoader.php');
require_once('../handlers/DocumentHandler.php');

use Hbase\Mutation;

$settings = get_option('tbes_viewer_settings', false);

if (!$settings || count($settings) < 3 || count($settings['thrift']) < 2) {
    echo 'Settings not properly configured for tBES Viewer.';
} else {
    $thrift_settings = $settings['thrift'];
    $hbase_connection = new HbaseConnection($thrift_settings['host'], $thrift_settings['port']);

    $feedback = trim($_GET['feedback']);
    $relation_id = trim($_GET['relation_id']);

    $row = $hbase_connection->getRow('relation', $relation_id);
    $column = $row->columns['feedback:has_feedback']->value;
    $has_feedback = filter_var($column, FILTER_VALIDATE_BOOLEAN);

    if (!$has_feedback) {
        $feedback_value;

        if ($feedback == 'positive') {
            $feedback_value = 'true';
        } else if ($feedback == 'negative') {
            $feedback_value = 'false';
        }

        echo $feedback_value;

        $mutations = array(
            new Mutation(array(
                'column' => 'feedback:has_feedback',
                'value' => 'true'
            )),
            new Mutation(array(
                'column' => 'feedback:feedback_is_positive',
                'value' => $feedback_value
            ))
        );

        $hbase_connection->mutateRow('relation', $relation_id, $mutations);
    } else {
        echo $row->columns['feedback:feedback_is_positive']->value;
    }
}
﻿/*
 * Author: Chris Håland <c.haaland@stud.uis.no>
 */

function addOverlay(topic_id) {
	var ajaxRequest;

    try {
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }

    ajaxRequest.onreadystatechange = function() {
        if(ajaxRequest.readyState == 4) {
            var ajaxResponse = ajaxRequest.responseText;

			var topicOverlay = document.createElement("div");
			topicOverlay.setAttribute("class", "topic_overlay");
			topicOverlay.innerHTML = ajaxResponse;

			var escKey = document.createElement("img");
			escKey.src = window.location.href + "wp-content/plugins/tbes-viewer/images/icon_02816.png";		
			escKey.onclick=removeOverlay;
			escKey.setAttribute("id", "overlay_esc");
			escKey.setAttribute("class", "overlay_esc");

	
			var overlay = document.getElementById("overlay");
			overlay.appendChild(topicOverlay);
			
			var overlayWidget = document.getElementById("overlay_widget");
			overlayWidget.appendChild(escKey);
        }
    }
	
    ajaxRequest.open("GET", window.location.href +
        "wp-content/plugins/tbes-viewer/lib/tbes/query/TopicOverlay.php?topic_id=" + topic_id, true);
    ajaxRequest.send(null);
    
   	var overlay = document.createElement("div");
	overlay.setAttribute("id", "overlay");
	overlay.setAttribute("class", "overlay");
	
	document.getElementById("page").appendChild(overlay);
}

function removeOverlay() {
	document.getElementById("page").removeChild(document.getElementById("overlay"));
}
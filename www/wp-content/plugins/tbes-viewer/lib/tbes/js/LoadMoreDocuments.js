/*
 * Author: Chris Håland <c.haaland@stud.uis.no>
 */

 function loadMoreDocuments(topic_id, relation_id, container_id) {
    var ajaxRequest;
    var documentContainer = document.getElementById(container_id);

    try {
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState == 4) {
            var ajaxResponse = ajaxRequest.responseText;
            var node = documentContainer.children[documentContainer.children.length - 1];
            if (node.nodeName == 'BUTTON') {
                documentContainer.removeChild(node);
            }

            documentContainer.innerHTML = documentContainer.innerHTML + ajaxResponse;
        }
    }
	
    ajaxRequest.open("GET", window.location.href +
        "wp-content/plugins/tbes-viewer/lib/tbes/query/LoadMoreDocuments.php?topic_id=" + topic_id + "&relation_id=" +
        relation_id + "&container_id=" + container_id, true);
    ajaxRequest.send(null);
 }

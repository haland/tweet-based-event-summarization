/*
 * Author: Chris H�land <c.haaland@stud.uis.no>
 */

function onSelectionChanged(root_topic_id, level) {
    var selection = document.getElementById("selection_" + level);
    var selected_topic_id = selection.options[selection.selectedIndex].value;

    var containerId = "overlay_topic_" + level;
    getDocumentsForTopicList(selected_topic_id, containerId);

    var childZero = parseInt(level) + 1;
    var topics = document.getElementById("tbes_topics").children;

    for (var i = childZero; i < topics.length; i++) {
        containerId = "overlay_topic_" + i;
        updateChildSelection(i, root_topic_id, selected_topic_id, containerId,
            document.getElementById("selection_" + i));
    }
}

function updateChildSelection(level, root_topic_id, parent_topic_id, containerId, selection) {
    selection.innerHTML = '';

    var ajaxRequest;

    try {
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState == 4) {
            selection.innerHTML = ajaxRequest.responseText;

            selection.selectedIndex = 0;
            getDocumentsForTopicList(selection.options[selection.selectedIndex].value, containerId);
        }
    }

    ajaxRequest.open("GET", window.location.href +
        "wp-content/plugins/tbes-viewer/lib/tbes/query/TopicSelection.php?level=" + level + "&root_topic_id=" +
        root_topic_id + "&parent_topic_id=" + parent_topic_id, true);
    ajaxRequest.send(null);
}

function getDocumentsForTopicList(topic_id, containerId) {
    var container = document.getElementById(containerId);
    removeAllChildren(container);

    var ajaxRequest;

    try {
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }

    ajaxRequest.onreadystatechange = function() {
        if(ajaxRequest.readyState == 4) {
            container.innerHTML = ajaxRequest.responseText;
        }
    }

    ajaxRequest.open("GET", window.location.href +
        "wp-content/plugins/tbes-viewer/lib/tbes/query/DocumentsForTopic.php?topic_id=" + topic_id + "&container_id=" +
        containerId, true);
    ajaxRequest.send(null);
}

function removeAllChildren(node) {
    while (node.lastChild) {
        node.removeChild(node.lastChild);
    }
}

function removeTopicColumns(id) {
    var topicColumnContainer = document.getElementById("topic_overlay");
    var topicColumnList = Array.prototype.slice.call(topicColumnContainer.children);

    for (var i = id; i < topicColumnList.length; i++) {
        topicColumnContainer.removeChild(topicColumnList[i]);
    }
}
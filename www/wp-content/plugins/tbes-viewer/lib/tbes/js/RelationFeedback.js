/*
 * Author: Chris H�land <c.haaland@stud.uis.no>
 */

function onPositiveVote(relation_id) {
    registerFeedback("positive", relation_id);
}

function onNegativeVote(relation_id) {
    registerFeedback("negative", relation_id);
}

function registerFeedback(feedback, relation_id) {
    var ajaxRequest;

    try {
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }

    ajaxRequest['relation_id'] = relation_id;
    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState == 4) {
            var response = ajaxRequest.responseText;
            var documents = document.getElementsByClassName(ajaxRequest['relation_id']);

            for (var i = 0; i < documents.length; i++) {
                var positiveInput = documents[i].children[3].children[0].children[0].children[1].children[0];
                positiveInput.style.visibility = response == "true" ? "visible" : "hidden";
                positiveInput.style.background = "#d4d0ba";
                positiveInput.disabled = true;

                var negativeInput = documents[i].children[3].children[0].children[0].children[2].children[0];
                negativeInput.style.visibility = response == "false" ? "visible" : "hidden";
                negativeInput.style.background = "#d4d0ba";
                negativeInput.disabled = true;
            }
        }
    }

    ajaxRequest.open("GET", window.location.href +
        "wp-content/plugins/tbes-viewer/lib/tbes/query/RelationFeedback.php?feedback=" + feedback + "&relation_id=" +
        relation_id, true);
    ajaxRequest.send(null);
}


<?php

class TopicHandler {
    private $topicTrees;
    private $mysql_connection;

    public function __construct(&$mysql_connection) {
        $this->topicTrees = array();
        $this->mysql_connection = $mysql_connection;

        $topicQuery = "SELECT id, name FROM topic WHERE parent_id IS NULL ORDER BY name";
        $topicResult = mysqli_query($this->mysql_connection, $topicQuery);

        while ($topic = mysqli_fetch_array($topicResult)) {
            $topic['children'] = array();

            $this->addChildTopics(&$topic);
            array_push($this->topicTrees, $topic);
        }
    }

    public function getTopicTrees() {
        return $this->topicTrees;
    }

    public function isParent($topic) {
        return count($topic['children']) < 1;
    }

    public function treeDepth($root) {
        $count = 0;

        foreach($root['children'] as $child) {
            $depth = $this->treeDepth($child);

            if ($count < $depth) {
                $count = $depth;
            }
        }

        return $count + 1;
    }

    public function getTopic($topic_id) {
        $topic;
        foreach ($this->topicTrees as $topicTree) {
            $topic = $this->getTopicFromTree($topicTree, $topic_id);
            if ($topic) {
                return $topic;
            }
        }
    }

    private function getTopicFromTree($topic, $topic_id) {
        if ($topic['id'] == $topic_id) {
            return $topic;
        } else {
            foreach ($topic['children'] as $child) {
                $result = $this->getTopicFromTree($child, $topic_id);
                if ($result) {
                    return $result;
                }
            }
        }
    }

    public function getTopicsOnLevel($root, $level) {
        $nodesOnLevel = array();
        $this->findTopicsOnLevel($root, $level, 0, $nodesOnLevel);

        return $nodesOnLevel;
    }

    private function findTopicsOnLevel($node, $level, $count, &$array) {
        $count = $count + 1;

        if ($count == $level) {
            array_push($array, $node);
        } else if ($count < $level) {
            foreach($node['children'] as $child) {
                $this->findTopicsOnLevel($child, $level, $count, $array);
            }
        }
    }

    private function addChildTopics($parent) {
        $childQuery = "SELECT id, name FROM topic WHERE parent_id = '" . $parent['id'] . "' ORDER BY uid";
        $childResult = mysqli_query($this->mysql_connection, $childQuery);

        while ($child = mysqli_fetch_array($childResult)) {
            $child['parent_id'] = $parent['id'];
            $child['children'] = array();

            $this->addChildTopics(&$child);
            array_push($parent['children'], $child);
        }
    }
}
<?php

class DocumentHandler {
    private $LONG_MAX = 9223372036854775807;

    private $n;
    private $hbase_connection;

    public function __construct(&$hbase_connection, $n) {
        $this->n = $n;
        $this->hbase_connection = $hbase_connection;
    }

    public function addDocumentsForTopic($topicId, $containerId) {
        $prefix = $topicId . '::';
        $relations = $this->hbase_connection->startsWith('relation', $prefix, $this->n);
        $documents = $this->getDocumentsFromRelations($relations, $topicId);

        $this->print_documents($documents, $relations, $topicId, $containerId);
    }

    public function addDocumentsForTopicFromRow($startRow, $topicId, $containerId) {
        $prefix = $topicId . '::';
        $relations = $this->hbase_connection->startFrom('relation', $prefix, $startRow, $this->n);
        $documents = $this->getDocumentsFromRelations($relations, $topicId);

        $this->print_documents($documents, $relations, $topicId, $containerId);
    }

    private function getDocumentsFromRelations(&$relations, $topicId) {
        $rows = array();
        foreach($relations as $relation) {
            list($relation_topic_id, $relation_document_id) = explode('::', $relation->row);
            $document_id = $this->LONG_MAX - intval($relation_document_id);

            if ($relation_topic_id === $topicId) {
                array_push($rows, $document_id);
            }
        }

        $documents = $this->hbase_connection->getRows('document', $rows);
        return $documents;
    }

    private function print_documents(&$documents, &$relations, $topicId, $containerId) {
        for($i = 0; $i < count($documents); $i++) {
            $document_id = $documents[$i]->row;
            $document_user = $documents[$i]->columns['publisher:username']->value;
            $document_timestamp = $documents[$i]->columns['document:created_at']->value;
            $document_text = $this->tbes_make_document_text($documents[$i]->columns['document:text']->value); ?>

            <div class="<?php echo $relations[$i]->row; ?>" id="tbes_document">
                <h1>
                    <a href="https://twitter.com/<?php echo $document_user; ?>">
                        <?php echo '@' . $document_user; ?>
                    </a>
                </h1>
                <p><?php echo $document_text; ?></p>
                <p style="font-size:x-small"><?php echo date('G:i:s \- jS M Y', strtotime($document_timestamp)); ?></p> <?php
                
                $hasFeedback = filter_var($relations[$i]->columns['feedback:has_feedback']->value, FILTER_VALIDATE_BOOLEAN);
                $feedbackIsPositive = $relations[$i]->columns['feedback:feedback_is_positive']->value; ?>
                <table style="border:0; margin-bottom:-12px; margin-right:-12px">
                    <tr>
                        <td style="width:256px"></td>
                        <td>
                            <input onclick="onPositiveVote('<?php echo $relations[$i]->row; ?>')" type="image"
                                src="<?php echo TBES_PLUGIN_IMAGE_URL . 'icon_14365.png';?>" width="46px"
                                alt="positive_registration" <?php echo $hasFeedback ? 'disabled' : '';?>
                                style="padding:0; <?php echo $hasFeedback ? 'background:#d4d0ba;' : '';?> visibility:<?php 
                                    echo $hasFeedback && $feedbackIsPositive == 'false' ? 'hidden' : 'visible'?>">
                        </td>
                        <td>
                            <input onclick="onNegativeVote('<?php echo $relations[$i]->row; ?>')" type="image"
                                src="<?php echo TBES_PLUGIN_IMAGE_URL . 'icon_14365_flipped.png';?>" width="46px"
                                alt="negative_registration" <?php echo $hasFeedback ? 'disabled' : '';?>
                                style="padding:0; <?php echo $hasFeedback ? 'background:#d4d0ba;' : '';?> visibility:<?php 
                                    echo $hasFeedback && $feedbackIsPositive == 'true' ? 'hidden' : 'visible'?>">
                        </td>
                    </tr>
                </table>
            </div> <?php
        }

        if (count($documents) == 0) { ?>
            <div id="tbes_document">
                <p><?php echo 'Excuse me! Nothing to see here! Move along...'; ?></p>
            </div> <?php
        } else if (count($documents) == $this->n) { ?>
            <button onclick="loadMoreDocuments('<?php echo $topicId; ?>',
                '<?php echo $relations[count($relations)-1]->row; ?>', '<?php echo $containerId; ?>')"
                    class="<?php '_loaderButton'; ?>" id="loadMoreButton">
                Load more documents...
            </button> <?php
        }
    }

    private function current_time() {
        return round(microtime(true) * 1000);
    }

    private function tbes_make_document_text($document) {
        $d1 = $this->tbes_make_links($document);
        $d2 = $this->tbes_make_mentions($d1);
        $d3 = $this->tbes_make_hashes($d2);
        return $d3;
    }

    private function tbes_make_links($tweet = ''){
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if( preg_match( $reg_exUrl, $tweet, $url ) )
            $tweet = preg_replace( $reg_exUrl, "<a target=\"_blank\" href=".$url[0].">{$url[0]}</a> ", $tweet);
        return $tweet;
    }

    private function tbes_make_mentions($tweet = ''){
        $regex = "/@[a-zA-Z0-9\_]*/";
        if( preg_match_all( $regex, $tweet, $matches ) ){
            foreach( (array) $matches[0] as $match ){
                $url = 'https://twitter.com/'.str_replace('@', '', $match);
                $tweet = str_replace( $match, "<a target=\"_blank\" href=".$url.">{$match}</a> ", $tweet);
            }
        }
        return $tweet;
    }

    private function tbes_make_hashes($tweet = ''){
        $regex = "/#[a-zA-Z0-9\_\-]*/";
        if( preg_match_all( $regex, $tweet, $matches ) ){
            foreach( (array) $matches[0] as $match ){
                $url = 'https://twitter.com/search?q=%23'.str_replace('#', '', $match).'&src=hash';
                $tweet = str_replace( $match, "<a target=\"_blank\" href=".$url.">{$match}</a> ", $tweet);
            }
        }
        return $tweet;
    }
}
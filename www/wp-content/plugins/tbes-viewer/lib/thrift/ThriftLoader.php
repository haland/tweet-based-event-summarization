<?php
    
/*
 * Author: Chris Håland <c.haaland@stud.uis.no>
 */    

$GLOBALS['THRIFT_LIB'] = dirname(__FILE__);
$GLOBALS['THRIFT_ROOT'] = $GLOBALS['THRIFT_LIB'] . '/Thrift';

require_once($GLOBALS['THRIFT_ROOT'] . '/Thrift.php');
require_once($GLOBALS['THRIFT_ROOT'] . '/ClassLoader/ThriftClassLoader.php');
    
use Thrift\ClassLoader\ThriftClassLoader;

$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', $THRIFT_LIB);
$loader->registerNamespace('Hbase', $THRIFT_ROOT . '/packages');
$loader->register();

require_once($GLOBALS['THRIFT_LIB'] . '/HbaseConnection.php');
<?php

/*
 * Author: Chris H�land <c.haaland@stud.uis.no>
 */

require_once($GLOBALS['THRIFT_ROOT'] . '/packages/Hbase/Types.php');

use Thrift\Transport\TSocket;
use Thrift\Transport\TBufferedTransport;
use Thrift\Protocol\TBinaryProtocol;

use Hbase\TScan;
use Hbase\HbaseClient;

class HbaseConnection {
    private $client;
    private $socket;
    private $protocol;
    private $transport;

    public function __construct($host, $port) {
        try {
            $this->socket = new TSocket($host, $port);
            $this->socket->setSendTimeout (2000); 
            $this->socket->setRecvTimeout (4000);
            
            $this->transport = new TBufferedTransport($this->socket);
            $this->protocol = new TBinaryProtocol($this->transport);
            $this->client = new HbaseClient($this->protocol);

            $this->transport->open();
        } catch (TException $e) {
            echo 'TException: ' . $e->__toString() . '<br>Error: ' . $e->getMessage() . '<br>';
        }
    }

    public function __destruct() {
        $this->transport->close();
    }
    
    public function getRow($table, $row) {
        $arr = $this->client->getRow($table, $row, array());
        return $arr[0];
    }

    public function getRows($table, $rows) {
        return $this->client->getRows($table, $rows, array());
    }
    
    /*
     * Example for mutateRow.
     *
     * use Hbase\Mutation;
     *
     * $table = 'table_name';
     * $row = 'row_key_of_row_to_alter';
     * $mutations = array(
     *      new Mutation(array(
     *          'column' => 'family:qualifier',
     *          'value' => $value1
     *      )),
     *      new Mutation(array(
     *          'column' => 'family:qualifier',
     *          'value' => $value2
     *      ))
     *  );
     *
     * $relations = hbase_connection->mutateRow($table, $row, $mutations);
     */
    public function mutateRow($table, $row, $mutations) {
        return $this->client->mutateRow($table, $row, $mutations, array());
    }

    /*
     * Example for using filter. '$n' does not need to be specified
     *
     * $filter = "RowFilter (=, 'binaryprefix:" . $prefix . "')";
     * $relations = hbase_connection->filter('table_name', $filter, $number_of_results_to_return);
     */
    public function filter($table, $filter, $n = 1000) {
        $scan = new TScan(array("filterString" => $filter));
        $scanner = $this->client->scannerOpenWithScan($table, $scan, array());
        $result = $this->client->scannerGetList($scanner, $n);

        $this->client->scannerClose($scanner);
        return $result;
    }

    /*
     * Example for using startFrom. '$n' and '$columns' does not need to be specified.
     *
     * $prefix = 'some_prefix';
     * $startAfterRow = 'row key from where to start'; //This row is not returned as part of the result.
     * $columns = array('family1:qualifier1', 'family1:qualifier2', ect.);
     * $result = hbase_connection->startFrom('table_name', $prefix, $startAfterRow, 
     *       $number_of_results_to_return, $columns);
     */
    public function startFrom($table, $prefix, $startAfterRow, $n = 1000, $columns = array()) {
        $scanner = $this->client->scannerOpen($table, $startAfterRow, $columns, array());
        $hbaseResults = $this->client->scannerGetList($scanner, $n + 1);
        
        $result = array();
        for($i = 1; $i < count($hbaseResults); $i++) {
            if(strpos($hbaseResults[$i]->row, $prefix, 0) === 0) {
                array_push($result, $hbaseResults[$i]);
            }
        }
        
        $this->client->scannerClose($scanner);
        return $result;
    }

    /*
     * Example for using startsWith. '$n' and '$columns' does not need to be specified.
     * $prefix = 'some_prefix';
     * $columns = array('family:qualifier', 'family:qualifier', ect.);
     * $result = hbase_connection->startsWith('table_name', $prefix, $number_of_results_to_return, $columns);
     */
    public function startsWith($table, $prefix, $n = 1000,  $columns = array()) {
        $scanner = $this->client->scannerOpenWithPrefix($table, $prefix, $columns, array());
        $result = $this->client->scannerGetList($scanner, $n);

        $this->client->scannerClose($scanner);
        return $result;
    }
}
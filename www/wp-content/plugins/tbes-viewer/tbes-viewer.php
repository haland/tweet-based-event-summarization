<?php
/**

 * Plugin Name: tBES - Viewer

 * Plugin URI: http://www.chrishaland.com

 * Description: Displays data from the Tweet Based Event Summarization application

 * Author: Chris Håland

 * Version: 0.0.2

 * Author URI: http://chrishaland.com

 * License: GNU

 */

require_once('lib/tbes/handlers/TopicHandler.php');
require_once('lib/tbes/handlers/DocumentHandler.php');
require_once('lib/thrift/ThriftLoader.php');

define('TBES_PLUGIN_NAME', 'tBES - Viewer');
define('TBES_PLUGIN_CSS_URL', trailingslashit(plugins_url('/css/', __FILE__) ));
define('TBES_PLUGIN_IMAGE_URL', trailingslashit(plugins_url('/images/', __FILE__) ));
define('TBES_PLUGIN_JS_URL', trailingslashit(plugins_url('/lib/tbes/js/', __FILE__) ));

class tbes_viewer extends WP_Widget {
    function tbes_viewer() {
        $options = array( 'description' => __('Presents tweets categorized by tBES.', TBES_PLUGIN_NAME));
        parent::WP_Widget(false, __('tBES - Viewer', TBES_PLUGIN_NAME), $options);
    }

    function widget($args, $instance) {
        extract($args, EXTR_SKIP);

        $settings = get_option('tbes_viewer_settings');

        if (!$settings || count($settings) < 3 || count($settings['thrift']) < 2 || count($settings['mysql']) < 3) {
            echo 'Settings not properly configured for tBES Viewer.';
        } else {
            $thrift_settings = $settings['thrift'];
            $hbase_connection = new HBaseConnection($thrift_settings['host'], $thrift_settings['port']);

            $mysql_settings = $settings['mysql'];
            $mysql_connection = @mysqli_connect($mysql_settings['host'], $mysql_settings['user'],
                $mysql_settings['password'], $mysql_settings['database']);

            $topic_handler = new TopicHandler($mysql_connection);
            $document_handler = new DocumentHandler($hbase_connection, $settings['document_per_topic_limit']);

            echo $before_widget; ?>

            <span class="tbes"> <?php
                $topicTrees = $topic_handler->getTopicTrees();
                foreach($topicTrees as $rootTopic) { ?>
                    <div class="tbes_topic">
                        <h3 class="tbes_topic_title" onclick="addOverlay('<?php echo $rootTopic['id']; ?>')"> <?php
                            echo $rootTopic['name']; ?>
                        </h3> <?php

                        $containerId = 'main_' . $rootTopic['id']; ?>

                        <div class="tbes_documents" id="<?php echo $containerId; ?>"> <?php
                            $document_handler->addDocumentsForTopic($rootTopic['id'], $containerId); ?>
                        </div>
                    </div> <?php
                } ?>
                </span> <?php

            echo $after_widget;
        }
    }

    function form($instance) {
        $settings = get_option('tbes_viewer_settings', false);

        if (!$settings || count($settings) < 3 || count($settings['thrift']) < 3 || count($settings['mysql']) < 3) {
            echo 'Please configure the tBES - Viewer settings <a href="' . site_url() .
                '/wp-admin/admin.php?page=tbes_viewer_settings">here</a>.';
        } else {
            echo 'There might be something wrong with the tBES - Viewer settings. To change them, click <a href="' .
                site_url() . '/wp-admin/admin.php?page=tbes_viewer_settings">here</a>.';
        }
    }
}

function tbes_viewer_admin_page() {
    $settings = get_option('tbes_viewer_settings');

    $mysql_settings = $settings['mysql'];

    $mysql_host = $mysql_settings['host'];
    $mysql_user = $mysql_settings['user'];
    $mysql_password = $mysql_settings['password'];
    $mysql_database = $mysql_settings['database'];

    $thrift_settings = $settings['thrift'];

    $thrift_host = $thrift_settings['host'];
    $thrift_port = $thrift_settings['port'];

    $document_per_topic_limit = $settings['document_per_topic_limit'];

    if ($_POST['tbes_viewer_save_settings']) {
        $thrift_host = trim($_POST['thrift_host']);
        $thrift_port = trim($_POST['thrift_port']);

        $mysql_host = trim($_POST['mysql_host']);
        $mysql_user = trim($_POST['mysql_user']);
        $mysql_password = trim($_POST['mysql_password']);
        $mysql_database = trim($_POST['mysql_database']);

        $document_per_topic_limit = trim($_POST['document_per_topic_limit']);

        $settings = array();
        $settings['document_per_topic_limit'] = $document_per_topic_limit;

        $settings['thrift'] = array(
            'host' => $thrift_host,
            'port' => $thrift_port
        );

        $settings['mysql'] = array(
            'host' => $mysql_host,
            'user' => $mysql_user,
            'password' => $mysql_password,
            'database' => $mysql_database
        );

        update_option('tbes_viewer_settings', $settings);
    } ?>

    <h3>tBES - Viewer : Settings</h3>
    <form id="tbes_viewer_settings" method="post" action="">
        <p>
            <label>Thrift host</label>
            <input type="text" class="widefat" name="thrift_host" id="thrift_host"
                   value="<?php echo $thrift_host ?>"/>
        </p>
        <p>
            <label>Thrift port</label>
            <input type="text" class="widefat" name="thrift_port" id="thrift_port"
                   value="<?php echo $thrift_port ?>"/>
        </p>

        <p>
            <label>MySQL hostname</label>
            <input type="text" class="widefat" name="mysql_host" id="mysql_host"
                   value="<?php echo $mysql_host ?>"/>
        </p>
        <p>
            <label>MySQL username</label>
            <input type="text" class="widefat" name="mysql_user" id="mysql_user"
                   value="<?php echo $mysql_user ?>"/>
        </p>
        <p>
            <label>MySQL password</label>
            <input type="password" class="widefat" name="mysql_password" id="mysql_password"
                   value="<?php echo $mysql_password ?>"/>
        </p>
        <p>
            <label>MySQL database</label>
            <input type="text" class="widefat" name="mysql_database" id="mysql_database"
                   value="<?php echo $mysql_database ?>"/>
        </p>

        <p>
            <label>Maximum number of documents per topic (set to -1 for unlimited)</label>
            <input type="text" class="widefat" name="document_per_topic_limit" id="document_per_topic_limit"
                   value="<?php echo $document_per_topic_limit ?>"/>
        </p>

        <p>
            <input type="submit" name="tbes_viewer_save_settings" id="tbes_viewer_save_settings"
                   value="save" type="button"/>
        </p>

    </form> <?php
}

function tbes_viewer_register_admin_menu_page(){
    add_menu_page( 'tBES - Viewer', 'tBES - Viewer', 'manage_options', 'tbes_viewer_settings',
        'tbes_viewer_admin_page' );
}

add_action( 'admin_menu', 'tbes_viewer_register_admin_menu_page' );

function tbes_viewer_load_scripts() {
    wp_register_style('tbes-viewer-style', TBES_PLUGIN_CSS_URL . 'style.css');
    wp_enqueue_style('tbes-viewer-style');

    wp_register_script('tbes-viewer-vote-script', TBES_PLUGIN_JS_URL . 'RelationFeedback.js');
    wp_enqueue_script('tbes-viewer-vote-script');

    wp_register_script('tbes-viewer-overlay-script', TBES_PLUGIN_JS_URL . 'Overlay.js');
    wp_enqueue_script('tbes-viewer-overlay-script');

    wp_register_script('tbes-viewer-load-more-documents-script', TBES_PLUGIN_JS_URL . 'LoadMoreDocuments.js');
    wp_enqueue_script('tbes-viewer-load-more-documents-script');

    wp_register_script('tbes-viewer-selection-change-script', TBES_PLUGIN_JS_URL . 'SelectionChange.js');
    wp_enqueue_script('tbes-viewer-selection-change-script');
}

add_action('wp_enqueue_scripts','tbes_viewer_load_scripts');

add_action('widgets_init', create_function('', 'return register_widget("tbes_viewer");'));


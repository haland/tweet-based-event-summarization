package com.chrishaland.tbes.model.utils.json;

import com.chrishaland.tbes.view.SwingConsole;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;

public class JSONReader {
    public static JSONObject readJSONObject(String filePath) {
        JSONParser parser = new JSONParser();

        try {
            Object object = parser.parse(new FileReader(filePath));
            JSONObject jsonObject = (JSONObject) object;

            return jsonObject;
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (ParseException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return null;
    }
}

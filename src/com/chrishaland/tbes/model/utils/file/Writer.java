package com.chrishaland.tbes.model.utils.file;

import com.chrishaland.tbes.view.SwingConsole;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

public class Writer {
    private BufferedWriter writer;

    public Writer(String fileName) {
        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    Files.newOutputStream(Paths.get(fileName)));
            writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        } catch (FileNotFoundException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public void writeLine(String s) {
        try {
            writer.write(s, 0, s.length());
            writer.newLine();
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

}
package com.chrishaland.tbes.model.utils.config;

import com.chrishaland.tbes.view.SwingConsole;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

public class ConfigurationReader {
    private Properties ini;
    private FileInputStream stream;

    public ConfigurationReader(String configFilePath) {
        try {
            ini = new Properties();
            stream = new FileInputStream(configFilePath);

            ini.load(stream);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (NullPointerException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            } catch (NullPointerException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }
        }
    }

    public String read(String key) {
        return ini.getProperty(key);
    }
}

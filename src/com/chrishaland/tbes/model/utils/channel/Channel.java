package com.chrishaland.tbes.model.utils.channel;

import java.util.concurrent.ArrayBlockingQueue;

public class Channel<E> extends ArrayBlockingQueue<E> {

    public Channel() {
        super(1);
    }

    public Channel(int bufferSize) {
        super(bufferSize);
    }
}
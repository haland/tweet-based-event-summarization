package com.chrishaland.tbes.model.twitter;

import com.chrishaland.tbes.model.utils.config.ConfigurationReader;

class TwitterConfiguration extends ConfigurationReader {
    public static final String TWITTER_CONFIG_FILE_PATH = "config/twitter_config.ini";

    public TwitterConfiguration() {
        super(TWITTER_CONFIG_FILE_PATH);
    }

    public String getAPIKey() {
        return read("api_key");
    }

    public String getAPISecret() {
        return read("api_secret");
    }

    public String getAccessToken() {
        return read("access_token");
    }

    public String getAccessTokenSecret() {
        return read("access_token_secret");
    }
}

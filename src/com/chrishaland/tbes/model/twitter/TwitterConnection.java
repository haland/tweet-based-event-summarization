package com.chrishaland.tbes.model.twitter;

import com.chrishaland.tbes.view.SwingConsole;
import twitter4j.FilterQuery;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;

public class TwitterConnection implements Runnable {
    private static TwitterConfiguration twitterConfiguration;

    static {
        twitterConfiguration = new TwitterConfiguration();
    }

    private Thread thread;
    private boolean isSampling;
    private FilterQuery filter;
    private Semaphore semaphore;
    private TwitterStream twitterStream;

    public TwitterConnection(StatusListener[] listeners, String[] languages, String[] trackers) {

        filter = new FilterQuery();
        filter.language(languages);
        filter.track(trackers);

        twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.setOAuthConsumer(twitterConfiguration.getAPIKey(), twitterConfiguration.getAPISecret());
        twitterStream.setOAuthAccessToken(new AccessToken(twitterConfiguration.getAccessToken(),
                twitterConfiguration.getAccessTokenSecret()));

        for (StatusListener listener : listeners)
            twitterStream.addListener(listener);

        semaphore = new Semaphore(0);
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        if (isSampling) {
            twitterStream.shutdown();
            isSampling = false;
        }
    }

    public void start() {
        if (!isSampling) {
            semaphore.release();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                semaphore.acquire();
                isSampling = true;

                if (filter != null) {
                    twitterStream.filter(filter);
                } else {
                    twitterStream.sample();
                }


            } catch (InterruptedException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }
        }
    }
}

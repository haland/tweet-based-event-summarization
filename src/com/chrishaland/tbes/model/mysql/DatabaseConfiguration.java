package com.chrishaland.tbes.model.mysql;

import com.chrishaland.tbes.model.utils.config.ConfigurationReader;

class DatabaseConfiguration extends ConfigurationReader {
    private static final String DATABASE_CONFIG_FILE_PATH = "config/database_config.ini";

    public DatabaseConfiguration() {
        super(DATABASE_CONFIG_FILE_PATH);
    }

    public String getPort() {
        return read("port");
    }

    public String getHostname() {
        return read("hostname");
    }

    public String getDatabase() {
        return read("database");
    }

    public String getUsername() {
        return read("username");
    }

    public String getPassword() {
        return read("password");
    }

    public String getPreTable() {
        return read("pre_table");
    }
}


package com.chrishaland.tbes.model.mysql;

import com.chrishaland.tbes.model.utils.config.ConfigurationReader;

public class MySQLConfiguration extends ConfigurationReader {
    private static final String MYSQL_CONFIG_FILE_PATH = "config/mysql_config.ini";

    public MySQLConfiguration() {
        super(MYSQL_CONFIG_FILE_PATH);
    }

    public String getPort() {
        return read("port");
    }

    public String getHostname() {
        return read("hostname");
    }

    public String getDatabase() {
        return read("database");
    }

    public String getUsername() {
        return read("username");
    }

    public String getPassword() {
        return read("password");
    }
}

package com.chrishaland.tbes.model.mysql;

import com.chrishaland.tbes.view.SwingConsole;

import java.sql.*;
import java.util.logging.Level;

public class MySQLConnection {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    private static MySQLConfiguration mySQLConfiguration;

    static {
        mySQLConfiguration = new MySQLConfiguration();
    }

    private Connection connection;
    private Object lock = new Object();

    public MySQLConnection() {
        try {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection("jdbc:mysql://" + mySQLConfiguration.getHostname() + ":" +
                            mySQLConfiguration.getPort() + "/" + mySQLConfiguration.getDatabase(),
                    mySQLConfiguration.getUsername(), mySQLConfiguration.getPassword()
            );
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (ClassNotFoundException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public Query executeQuery(String query) {
        synchronized (lock) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                return new Query(resultSet, statement);
            } catch (SQLException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }

            return null;
        }
    }

    public Update executeUpdate(String query) {
        synchronized (lock) {
            try {
                Statement statement = connection.createStatement();
                int updates = statement.executeUpdate(query);

                return new Update(updates, statement);
            } catch (SQLException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }

            return null;
        }
    }

    public void close() {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public class Query {
        private ResultSet resultSet;
        private Statement statement;

        public Query(ResultSet resultSet, Statement statement) {
            this.resultSet = resultSet;
            this.statement = statement;
        }

        public ResultSet getResultSet() {
            return resultSet;
        }

        public void close() {
            try {
                if (statement != null)
                    statement.close();
                if (resultSet != null)
                    resultSet.close();
            } catch (SQLException e) {
                //DO NOTHING
            }
        }
    }

    public class Update {
        private int update;
        private Statement statement;

        public Update(int update, Statement statement) {
            this.update = update;
            this.statement = statement;
        }

        public int getUpdate() {
            return update;
        }

        public void close() {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                //DO NOTHING
            }
        }
    }
}

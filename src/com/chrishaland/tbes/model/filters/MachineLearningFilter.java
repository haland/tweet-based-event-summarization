package com.chrishaland.tbes.model.filters;

import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.relation.Relation;
import com.chrishaland.tbes.model.data.relation.TrackerData;
import com.chrishaland.tbes.model.data.topic.Topic;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.hbase.tables.DocumentTable;
import com.chrishaland.tbes.model.hbase.tables.RelationTable;
import com.chrishaland.tbes.model.utils.file.Writer;
import com.chrishaland.tbes.view.SwingConsole;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import weka.classifiers.Classifier;
import weka.classifiers.trees.RandomForest;
import weka.core.*;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MachineLearningFilter {
    private static final String MODEL_FOLDER_PATH = "data/weka/";
    private static final String MODEL_FILE_NAME = "model.arff";
    private static final String RELATIONS_FILE_NAME = "relations.arff";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final FastVector attributeList;
    private static final FastVector relationClassifications;

    private static final Attribute relationAttribute;
    private static final Attribute childrenTrackersAttribute = new Attribute("children_trackers");
    private static final Attribute negativeTrackersAttribute = new Attribute("negative_trackers");
    private static final Attribute positiveTrackersAttribute = new Attribute("positive_trackers");
    private static final Attribute positiveTrackersAsHashtagsAttribute = new Attribute("positive_trackers_as_hashtags");

    static {
        relationClassifications = new FastVector(2);
        relationClassifications.addElement("positive");
        relationClassifications.addElement("negative");

        relationAttribute = new Attribute("relation", relationClassifications);

        attributeList = new FastVector(5);
        attributeList.addElement(positiveTrackersAttribute);
        attributeList.addElement(positiveTrackersAsHashtagsAttribute);
        attributeList.addElement(negativeTrackersAttribute);
        attributeList.addElement(childrenTrackersAttribute);
        attributeList.addElement(relationAttribute);
    }

    private TopicMap topicMap;
    private Instances modelData;
    private Classifier classifier;
    private ConverterUtils.DataSource model;
    private Writer modelWriter, relationsWriter;

    public MachineLearningFilter(TopicMap topicMap) {
        this.topicMap = topicMap;

        File modelFile = new File(MODEL_FOLDER_PATH + MODEL_FILE_NAME);
        File relationsFile = new File(MODEL_FOLDER_PATH + RELATIONS_FILE_NAME);
        if (modelFile.exists() || relationsFile.exists()) {
            modelFile.delete();
            relationsFile.delete();
        }

        SwingConsole.getInstance().log(Level.INFO, "Building machine learning model");
        rebuild();
    }

    public int getCurrentModelInstancesCount() {
        return modelData.numInstances();
    }

    public boolean isClassifierActive() {
        return classifier != null;
    }

    public Relation[] filter(Relation[] relations) {
        Relation previousPassedRelation = null;
        ArrayList<Relation> machineLearningRelationList = new ArrayList<Relation>();
        for (Relation relation : relations) {
            if (relation.getTopic().isRoot() && classify(relation.getDocument(), relation.getTopic())) {
                previousPassedRelation = relation;
                machineLearningRelationList.add(relation);
            } else if (!relation.getTopic().isRoot() && previousPassedRelation != null &&
                    relation.getTopic().getParent().equals(previousPassedRelation.getTopic())) {
                previousPassedRelation = relation;
                machineLearningRelationList.add(relation);
            } else {
                previousPassedRelation = null;
            }
        }

        if (!machineLearningRelationList.isEmpty()) {
            return machineLearningRelationList.toArray(new Relation[machineLearningRelationList.size()]);
        }

        return null;
    }

    private boolean classify(Document document, Topic topic) {
        try {
            TrackerData trackerData = getDocumentMatches(document.getTextWithoutURLs(), topic);

            Instance instance = new SparseInstance(5);
            instance.setValue(positiveTrackersAttribute, trackerData.getPositiveTrackers());
            instance.setValue(positiveTrackersAsHashtagsAttribute, trackerData.getPositiveTrackersAsHashtags());
            instance.setValue(negativeTrackersAttribute, trackerData.getNegativeTrackers());
            instance.setValue(childrenTrackersAttribute, trackerData.getChildrenTrackers());
            instance.setMissing(relationAttribute);

            Instances testData = new Instances("testData", attributeList, 0);
            testData.add(instance);
            testData.setClassIndex(testData.numAttributes() - 1);

            double result = classifier.classifyInstance(testData.firstInstance());
            int nominalValue = (int) result;

            return nominalValue == 0;
        } catch (Exception e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            return false;
        }
    }

    public void rebuild() {
        moveCurrentModel();

        modelWriter = new Writer(MODEL_FOLDER_PATH + MODEL_FILE_NAME);
        relationsWriter = new Writer(MODEL_FOLDER_PATH + RELATIONS_FILE_NAME);

        writeModelAttributes();
        writeRelationsAttributes();

        writeRelations();

        modelWriter.close();
        relationsWriter.close();

        rebuildClassifier();
    }

    private void rebuildClassifier() {
        try {
            model = new ConverterUtils.DataSource(MODEL_FOLDER_PATH + MODEL_FILE_NAME);
            modelData = model.getDataSet();
            modelData.setClassIndex(modelData.numAttributes()-1);

            classifier = new RandomForest();
            classifier.buildClassifier(modelData);
        } catch (Exception e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    private void writeRelations() {
        SingleColumnValueFilter hasFeedbackFilter = new SingleColumnValueFilter(Bytes.toBytes("feedback"),
                Bytes.toBytes("has_feedback"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes("true"));

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(hasFeedbackFilter);

        ResultScanner resultScanner = RelationTable.filter(filterList);
        Iterator<Result> i = resultScanner.iterator();
        while(i.hasNext()) {
            Result relation = i.next();
            String topicId = new String(relation.getValue(Bytes.toBytes("relation"), Bytes.toBytes("topic_id")));
            String documentId = new String(relation.getValue(Bytes.toBytes("relation"), Bytes.toBytes("document_id")));

            boolean feedbackIsPositive = Boolean.valueOf(new String(relation.getValue(Bytes.toBytes("feedback"),
                    Bytes.toBytes("feedback_is_positive"))));

            Topic topic = topicMap.get(topicId);
            Result document = DocumentTable.get(documentId);
            String documentText = new String(document.getValue(Bytes.toBytes("document"), Bytes.toBytes("text")));

            TrackerData trackerData = getDocumentMatches(documentText, topic);

            String entry = String.valueOf(trackerData.getPositiveTrackers()) + ", " +
                    String.valueOf(trackerData.getPositiveTrackersAsHashtags()) + ", " +
                    String.valueOf(trackerData.getNegativeTrackers()) + ", " +
                    String.valueOf(trackerData.getChildrenTrackers()) + ", " +
                    (feedbackIsPositive ? "positive" : "negative");
            modelWriter.writeLine(entry);
            relationsWriter.writeLine(documentId + ", " + topicId);
        }
    }

    private TrackerData getDocumentMatches(String documentText, Topic topic) {
        while(!topic.isRoot()) {
            topic = topic.getParent();
        }

        TrackerData trackerData = new TrackerData();

        Pattern positiveRegexPattern = Pattern.compile(topic.getRegex());
        Matcher positiveRegexMatcher = positiveRegexPattern.matcher(documentText);
        while(positiveRegexMatcher.find()) {
            trackerData.incrementPositiveTrackers();
        }

        Pattern positiveRegexAsHashtagPattern = Pattern.compile(topic.getRegex());
        Matcher positiveRegexAsHashtagMatcher = positiveRegexAsHashtagPattern.matcher(documentText);
        while(positiveRegexAsHashtagMatcher.find()) {
            trackerData.incrementPositiveTrackersAsHashtags();
        }

        Pattern positiveTrackersPattern = Pattern.compile(topic.getPositiveTrackersRegex());
        Matcher positiveTrackersMatcher = positiveTrackersPattern.matcher(documentText);
        while(positiveTrackersMatcher.find()) {
            trackerData.incrementPositiveTrackers();
        }

        Pattern positiveTrackersAsHashtagsPattern = Pattern.compile("#" + topic.getPositiveTrackersRegex());
        Matcher positiveTrackersAsHashtagsMatcher = positiveTrackersAsHashtagsPattern.matcher(documentText);
        while(positiveTrackersAsHashtagsMatcher.find()) {
            trackerData.incrementPositiveTrackersAsHashtags();
        }

        Pattern negativeTrackersPattern = Pattern.compile(topic.getNegativeTrackersRegex());
        Matcher negativeTrackersMatcher = negativeTrackersPattern.matcher(documentText);
        while(negativeTrackersMatcher.find()) {
            trackerData.incrementNegativeTrackers();
        }

        trackerData.setChildrenTrackers(countChildrenTrackers(topic, documentText));

        return trackerData;
    }

    private int countChildrenTrackers(Topic topic, String documentText) {
        int level = 0;

        for (Topic child : topic.getChildren()) {
            Pattern childTrackerPattern = Pattern.compile(topic.getRegex());
            Matcher childTrackerMatcher = childTrackerPattern.matcher(documentText);

            int newLevel;
            if (childTrackerMatcher.find()) {
                newLevel = countChildrenTrackers(child, documentText);
            } else {
                newLevel = topic.getLevel() - 1;
            }

            if (newLevel > level) {
                level = newLevel;
            }
        }

        return level;
    }

    private void writeModelAttributes() {
        modelWriter.writeLine("@relation document");
        modelWriter.writeLine("@attribute positive_trackers numeric");
        modelWriter.writeLine("@attribute positive_trackers_as_hashtags numeric");
        modelWriter.writeLine("@attribute negative_trackers numeric");
        modelWriter.writeLine("@attribute children_trackers numeric");
        modelWriter.writeLine("@attribute relation {positive, negative}");

        modelWriter.writeLine("");
        modelWriter.writeLine("@data");
    }

    private void writeRelationsAttributes() {
        relationsWriter.writeLine("@relation relation");
        relationsWriter.writeLine("@attribute document_id numeric");
        relationsWriter.writeLine("@attribute topic_id nominal");

        relationsWriter.writeLine("");
        relationsWriter.writeLine("@data");
    }

    private void moveCurrentModel() {
        if (modelWriter != null) {
            Path modelSrc = new File(MODEL_FOLDER_PATH + MODEL_FILE_NAME).toPath();
            Path relationsSrc = new File(MODEL_FOLDER_PATH + RELATIONS_FILE_NAME).toPath();

            try {
                Files.move(modelSrc, modelSrc.resolveSibling(getDateFormat() + "-" + MODEL_FILE_NAME),
                        StandardCopyOption.REPLACE_EXISTING);
                Files.move(relationsSrc, relationsSrc.resolveSibling(getDateFormat() + "-" + RELATIONS_FILE_NAME));
            } catch (IOException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            } finally {
                new File(MODEL_FOLDER_PATH + MODEL_FILE_NAME).delete();
                new File(MODEL_FOLDER_PATH + RELATIONS_FILE_NAME).delete();
            }
        }
    }

    private String getDateFormat() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(Calendar.DATE, -1);
        return DATE_FORMAT.format(calendar.getTime());
    }
}

package com.chrishaland.tbes.model.filters;

import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.relation.Relation;
import com.chrishaland.tbes.model.data.topic.Topic;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;

import java.util.regex.Pattern;

public class RegexFilter {
    private TopicMap topicMap;

    public RegexFilter(TopicMap topicMap) {
        this.topicMap = topicMap;
    }

    public Relation[] filter(Document document) {
        Relation[] relations = null;
        Topic[] rootTopics = topicMap.getRootTopics();

        for (Topic topic : rootTopics) {
            Relation[] newRelations = createTopicToDocumentRelations(document, topic);

            if (newRelations != null) {
                if (relations != null) {
                    Relation[] tempRelations = relations;
                    relations = new Relation[tempRelations.length + newRelations.length];

                    System.arraycopy(tempRelations, 0, relations, 0, tempRelations.length);
                    System.arraycopy(newRelations, 0, relations, tempRelations.length, newRelations.length);
                } else {
                    relations = newRelations;
                }
            }
        }

        return relations;
    }

    private Relation[] createTopicToDocumentRelations(Document document, Topic topic) {
        Relation[] relations = null;
        Pattern p = Pattern.compile(topic.getRegex());

        if (p.matcher(document.getTextWithoutURLs()).find()) {
            Relation[] lowerTierRelations = null;
            Relation[] newRelation = {new Relation(topic, document, false, false)};

            if (!topic.getChildren().isEmpty()) {
                for (Topic child : topic.getChildren()) {
                    Relation[] newLowerTierRelations = createTopicToDocumentRelations(document, child);

                    if (newLowerTierRelations != null) {
                        if (lowerTierRelations != null) {
                            Relation[] tempLowerTierRelations = lowerTierRelations;
                            lowerTierRelations = new Relation[tempLowerTierRelations.length +
                                    newLowerTierRelations.length];

                            System.arraycopy(tempLowerTierRelations, 0, lowerTierRelations, 0,
                                    tempLowerTierRelations.length);
                            System.arraycopy(newLowerTierRelations, 0, lowerTierRelations,
                                    tempLowerTierRelations.length, newLowerTierRelations.length);
                        } else {
                            lowerTierRelations = newLowerTierRelations;
                        }
                    }
                }
            }

            if (lowerTierRelations != null) {
                if (relations != null) {
                    Relation[] tempRelations = relations;
                    relations = new Relation[tempRelations.length + newRelation.length + lowerTierRelations.length];

                    System.arraycopy(tempRelations, 0, relations, 0, tempRelations.length);
                    System.arraycopy(newRelation, 0, relations, tempRelations.length, newRelation.length);
                    System.arraycopy(lowerTierRelations, 0, relations, tempRelations.length + newRelation.length,
                            lowerTierRelations.length);
                } else {
                    relations = new Relation[newRelation.length + lowerTierRelations.length];

                    System.arraycopy(newRelation, 0, relations, 0, newRelation.length);
                    System.arraycopy(lowerTierRelations, 0, relations, newRelation.length, lowerTierRelations.length);
                }
            } else {
                relations = newRelation;
            }
        }

        return relations;
    }
}

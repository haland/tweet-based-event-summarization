package com.chrishaland.tbes.model.data;

import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.publisher.Publisher;
import com.chrishaland.tbes.model.data.relation.Relation;

public class Construct {
    private Document document;
    private Publisher publisher;
    private Relation[] relations;

    public Construct(Document document, Publisher publisher, Relation[] relations) {
        this.document = document;
        this.publisher = publisher;
        this.relations = relations;
    }

    public Document getDocument() {
        return document;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public Relation[] getRelations() {
        return relations;
    }
}

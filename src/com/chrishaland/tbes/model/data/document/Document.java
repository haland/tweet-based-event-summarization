package com.chrishaland.tbes.model.data.document;

import com.chrishaland.tbes.model.data.publisher.Publisher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Document {
    private static DateFormat DATE_FORMAT = null;

    static {
        DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    }

    private long id;
    private Publisher publisher;
    private String text, createdAt;
    private int retweetCount, favoriteCount;

    public Document(long id, String text, Date createdAt, int retweetCount, int favoriteCount, Publisher publisher)
            throws IllegalArgumentException {

        if (text == null || createdAt == null || publisher == null) {
            throw new IllegalArgumentException("Arguments can not be 'null'");
        }

        this.id = id;
        this.text = text;
        this.publisher = publisher;
        this.retweetCount = retweetCount;
        this.favoriteCount = favoriteCount;
        this.createdAt = DATE_FORMAT.format(createdAt);
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getTextWithoutURLs() {
        String urlRegex = "\\(?\\b(((ftp|https*)://(www[.])*)|(www[.]))[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        String textWithoutURLs = text.replaceAll(urlRegex, "");
        return textWithoutURLs;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public Publisher getPublisher() {
        return publisher;
    }
}

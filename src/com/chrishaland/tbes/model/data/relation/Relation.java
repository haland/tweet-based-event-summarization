package com.chrishaland.tbes.model.data.relation;

import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.topic.Topic;

public class Relation {
    private static final String ROW_KEY_DELIMITER = "::";

    private String id;
    private Topic topic;
    private Document document;
    private boolean hasFeedback, isFeedbackPositive;

    public Relation(Topic topic, Document document, boolean hasFeedback, boolean isFeedbackPositive) {
        this.topic = topic;
        this.document = document;

        this.id = topic.getId() + ROW_KEY_DELIMITER + (Long.MAX_VALUE - document.getId());

        this.hasFeedback = hasFeedback;
        this.isFeedbackPositive = isFeedbackPositive;
    }

    public String getId() {
        return id;
    }

    public Topic getTopic() {
        return topic;
    }

    public Document getDocument() {
        return document;
    }

    public boolean hasFeedback() {
        return hasFeedback;
    }

    public boolean isFeedbackPositive() {
        return isFeedbackPositive;
    }
}

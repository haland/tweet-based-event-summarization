package com.chrishaland.tbes.model.data.relation;

public class TrackerData {
    private int positiveTrackers, negativeTrackers, positiveTrackersAsHashtags, childrenTrackers;

    public TrackerData() {
        positiveTrackers = negativeTrackers = positiveTrackersAsHashtags = childrenTrackers = 0;
    }

    public void incrementPositiveTrackers() {
        positiveTrackers++;
    }

    public void incrementNegativeTrackers() {
        negativeTrackers++;
    }

    public void incrementPositiveTrackersAsHashtags() {
        positiveTrackersAsHashtags++;
    }

    public void setChildrenTrackers(int childrenTrackers) {
        this.childrenTrackers = childrenTrackers;
    }

    public int getPositiveTrackers() {
        return positiveTrackers;
    }

    public int getNegativeTrackers() {
        return negativeTrackers;
    }

    public int getPositiveTrackersAsHashtags(){
        return positiveTrackersAsHashtags;
    }

    public int getChildrenTrackers() {
        return childrenTrackers;
    }
}

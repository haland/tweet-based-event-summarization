package com.chrishaland.tbes.model.data.publisher;

public class Publisher {
    private long id;
    private int tweetCount, followersCount;
    private String name, username, url;

    public Publisher(long id, String name, String username, String url, int tweetCount, int followersCount)
            throws IllegalArgumentException {

        if (url == null || name == null || username == null) {
            throw new IllegalArgumentException("Arguments can not be 'null'");
        }

        this.id = id;
        this.url = url;
        this.name = name;
        this.username = username;
        this.tweetCount = tweetCount;
        this.followersCount = followersCount;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getUrl() {
        return url;
    }

    public int getTweetCount() {
        return tweetCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }
}
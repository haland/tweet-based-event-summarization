package com.chrishaland.tbes.model.data.topic.tree;

import com.chrishaland.tbes.model.data.topic.Topic;
import com.chrishaland.tbes.model.utils.json.JSONReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

public class TopicMap extends TreeMap<String, Topic> {
    private static final String JSON_FILE_PATH = "data/json/topics.json";

    public TopicMap() {
        JSONObject jsonObject = JSONReader.readJSONObject(JSON_FILE_PATH);
        JSONArray jsonArray = (JSONArray) jsonObject.get("topics");

        Iterator<Object> i = jsonArray.iterator();
        while (i.hasNext()) {
            importTopics((JSONObject) i.next(), null);
        }
    }

    public Topic[] getRootTopics() {
        ArrayList<Topic> rootList = new ArrayList<Topic>();
        Iterator<Topic> i = this.values().iterator();

        while(i.hasNext()) {
            Topic t = i.next();
            if (t.isRoot()) {
                rootList.add(t);
            }
        }

        if (!rootList.isEmpty()) {
            return rootList.toArray(new Topic[rootList.size()]);
        }

        return null;
    }

    private void importTopics(JSONObject o, Topic parentTopic) {
        Topic topic;
        String name = (String) o.get("name");
        String regex = (String) o.get("regex");

        if (parentTopic == null) {
            String positiveTrackersRegex = (String) o.get("positive_trackers");
            String negativeTrackersRegex = (String) o.get("negative_trackers");

            topic = new Topic(name, regex, positiveTrackersRegex, negativeTrackersRegex);
        } else {
            topic = new Topic(name, regex, parentTopic);
            parentTopic.addChild(topic);
        }

        put(topic.getId(), topic);

        JSONArray childTopics = (JSONArray) o.get("topics");
        if (childTopics != null) {
            Iterator<Object> i = childTopics.iterator();
            while (i.hasNext()) {
                importTopics((JSONObject) i.next(), topic);
            }
        }
    }
}

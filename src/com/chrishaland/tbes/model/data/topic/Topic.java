package com.chrishaland.tbes.model.data.topic;

import java.text.Normalizer;
import java.util.ArrayList;

public class Topic {
    private Topic parent;
    private ArrayList<Topic> children;
    private String id, name, regex, positiveTrackersRegex, negativeTrackersRegex;

    public Topic(String name, String regex, String positiveTrackersRegex, String negativeTrackersRegex) {
        this.name = name;
        this.regex = regex;
        this.positiveTrackersRegex = positiveTrackersRegex;
        this.negativeTrackersRegex = negativeTrackersRegex;

        id = getSimplisticName(name);
        children = new ArrayList<Topic>();
    }

    public Topic(String name, String regex, Topic parent) {
        this.name = name;
        this.regex = regex;
        this.parent = parent;

        id = parent.getId() + "_" + getSimplisticName(name);
        children = new ArrayList<Topic>();
    }

    public boolean isRoot() {
        return parent == null;
    }

    public int getLevel() {
        int count = 0;
        Topic current = this;
        while(current.getParent() != null) {
            count++;
            current = current.getParent();
        }

        return count;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRegex() {
        return regex;
    }

    public String getPositiveTrackersRegex() {
        return positiveTrackersRegex;
    }

    public String getNegativeTrackersRegex() {
        return negativeTrackersRegex;
    }

    public Topic getParent() {
        return parent;
    }

    public ArrayList<Topic> getChildren() {
        return children;
    }

    public void addChild(Topic child) {
        children.add(child);
    }

    private static String getSimplisticName(String name) {
        String normalizedName = Normalizer.normalize(name, Normalizer.Form.NFD);
        String plainName = normalizedName.replaceAll("[^\\sa-zA-Z0-9]", "");
        String lowerCaseName = plainName.toLowerCase();
        String simplisticName = lowerCaseName.replace(" ", "_");

        return simplisticName;
    }
}

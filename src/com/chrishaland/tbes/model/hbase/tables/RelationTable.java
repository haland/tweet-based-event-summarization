package com.chrishaland.tbes.model.hbase.tables;

import com.chrishaland.tbes.model.hbase.HBaseConnection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.util.Bytes;

public class RelationTable {
    private static final String TABLE_NAME = "relation";

    private static final String[] FAMILIES = {
            "relation", "feedback"
    };

    private static final String[] QUALIFIERS = {
            "topic_id", "document_id", "has_feedback", "feedback_is_positive"
    };

    private static final byte[][] INSERT_FAMILIES = {
            Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[1]),
            Bytes.toBytes(FAMILIES[1])
    };

    private static final byte[][] INSERT_QUALIFIERS = {
            Bytes.toBytes(QUALIFIERS[0]), Bytes.toBytes(QUALIFIERS[1]), Bytes.toBytes(QUALIFIERS[2]),
            Bytes.toBytes(QUALIFIERS[3])
    };

    private static HTable relationTable; //Do not use directly

    private static HTable getRelationTable() {
        if (relationTable == null) {
            relationTable = HBaseConnection.getTable(TABLE_NAME);
        }
        return relationTable;
    }

    public static long countFeedback() {
        return HBaseConnection.count(TABLE_NAME, FAMILIES[1], QUALIFIERS[2], String.valueOf(true));
    }

    public static void create() {
        HBaseConnection.createTable(TABLE_NAME, FAMILIES);
    }

    public static void remove() {
        HBaseConnection.removeTable(TABLE_NAME);
    }

    public static void insert(String relation_id, String topic_id, long document_id, boolean has_feedback,
                              boolean feedback_is_positive) {

        byte[][] VALUES = {
                Bytes.toBytes(topic_id), Bytes.toBytes(String.valueOf(document_id)),
                Bytes.toBytes(String.valueOf(has_feedback)), Bytes.toBytes(String.valueOf(feedback_is_positive))
        };

        HBaseConnection.put(getRelationTable(), Bytes.toBytes(relation_id), INSERT_FAMILIES, INSERT_QUALIFIERS, VALUES);
    }

    public static ResultScanner filter(FilterList filterList) {
        return HBaseConnection.scan(getRelationTable(), filterList);
    }

    public static boolean rowExists(String relation_id) {
        return HBaseConnection.rowExists(getRelationTable(), relation_id);
    }

    public static Result get(String rowKey) {
        return HBaseConnection.get(getRelationTable(), rowKey);
    }
}

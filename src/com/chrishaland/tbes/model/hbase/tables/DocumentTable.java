package com.chrishaland.tbes.model.hbase.tables;

import com.chrishaland.tbes.model.hbase.HBaseConnection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.util.Bytes;

public class DocumentTable {
    private static final String TABLE_NAME = "document";

    private static final String[] FAMILIES = {
            "document", "publisher",
    };

    private static final byte[][] INSERT_FAMILIES = {
            Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]),
            Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[1]), Bytes.toBytes(FAMILIES[1])
    };

    private static final byte[][] INSERT_QUALIFIERS = {
            Bytes.toBytes("text"), Bytes.toBytes("created_at"), Bytes.toBytes("retweet_count"),
            Bytes.toBytes("favorite_count"), Bytes.toBytes("id"), Bytes.toBytes("username")
    };

    private static HTable documentTable; //Do not use directly

    private static HTable getDocumentTable() {
        if (documentTable == null) {
            documentTable = HBaseConnection.getTable(TABLE_NAME);
        }
        return documentTable;
    }

    public static void create() {
        HBaseConnection.createTable(TABLE_NAME, FAMILIES);
    }

    public static void remove() {
        HBaseConnection.removeTable(TABLE_NAME);
    }

    public static void insert(long document_id, String document_text, String created_at, int retweet_count,
                              int favorite_count, long user_id, String username) {

        byte[][] VALUES = {
                Bytes.toBytes(document_text), Bytes.toBytes(created_at), Bytes.toBytes(String.valueOf(retweet_count)),
                Bytes.toBytes(String.valueOf(favorite_count)), Bytes.toBytes(String.valueOf(user_id)),
                Bytes.toBytes(username)
        };

        HBaseConnection.put(getDocumentTable(), Bytes.toBytes(String.valueOf(document_id)), INSERT_FAMILIES,
                INSERT_QUALIFIERS, VALUES);
    }

    public static ResultScanner filter(FilterList filterList) {
        return HBaseConnection.scan(getDocumentTable(), filterList);
    }

    public static Result get(String rowKey) {
        return HBaseConnection.get(getDocumentTable(), rowKey);
    }
}

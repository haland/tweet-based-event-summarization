package com.chrishaland.tbes.model.hbase.tables;

import com.chrishaland.tbes.model.hbase.HBaseConnection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.util.Bytes;

public class PublisherTable {
    private static final String TABLE_NAME = "publisher";

    private static final String[] FAMILIES = {
            "publisher"
    };

    private static final byte[][] INSERT_FAMILIES = {
            Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]),
            Bytes.toBytes(FAMILIES[0]), Bytes.toBytes(FAMILIES[0]),
    };

    private static final byte[][] INSERT_QUALIFIERS = {
            Bytes.toBytes("name"), Bytes.toBytes("username"), Bytes.toBytes("url"), Bytes.toBytes("tweets_count"),
            Bytes.toBytes("followers_count")
    };

    private static HTable userTable; //Do not use directly

    private static HTable getUserTable() {
        if (userTable == null) {
            userTable = HBaseConnection.getTable(TABLE_NAME);
        }
        return userTable;
    }

    public static void create() {
        HBaseConnection.createTable(TABLE_NAME, FAMILIES);
    }

    public static void remove() {
        HBaseConnection.removeTable(TABLE_NAME);
    }

    public static void insert(long user_id, String name, String username, String url, int tweets_count,
                              int followers_count) {

        byte[][] VALUES = {
                Bytes.toBytes(name), Bytes.toBytes(username), Bytes.toBytes(url),
                Bytes.toBytes(String.valueOf(tweets_count)), Bytes.toBytes(String.valueOf(followers_count))
        };

        HBaseConnection.put(getUserTable(), Bytes.toBytes(String.valueOf(user_id)), INSERT_FAMILIES, INSERT_QUALIFIERS,
                VALUES);
    }

    public static ResultScanner filter(FilterList filterList) {
        return HBaseConnection.scan(getUserTable(), filterList);
    }

    public static Result get(String rowKey) {
        return HBaseConnection.get(getUserTable(), rowKey);
    }
}
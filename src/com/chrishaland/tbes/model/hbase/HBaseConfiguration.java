package com.chrishaland.tbes.model.hbase;

import com.chrishaland.tbes.model.utils.config.ConfigurationReader;
import org.apache.hadoop.conf.Configuration;

public class HBaseConfiguration {
    private static final long NULL = -1;
    private static final String HBASE_CONFIG_FILE_PATH = "config/hbase_config.ini";

    public static Configuration createConfiguration() {
        return createConfiguration(NULL, NULL);
    }

    public static Configuration createConfiguration(long timeout, long caching) {
        Configuration config = new Configuration();
        ConfigurationReader configReader = new ConfigurationReader(HBASE_CONFIG_FILE_PATH);

        config.clear();
        config.set("hbase.zookeeper.quorum", configReader.read("host_ip"));
        config.set("hbase.zookeeper.property.clientPort", configReader.read("zookeeper_port"));
        config.set("hbase.master", configReader.read("host_ip") + ":" + configReader.read("hbase_port"));

        if (timeout != NULL) {
            config.setLong("hbase.rpc.timeout", timeout);
        }

        if (caching != NULL) {
            config.setLong("hbase.client.scanner.caching", caching);
        }

        return org.apache.hadoop.hbase.HBaseConfiguration.create(config);
    }
}

package com.chrishaland.tbes.model.hbase;

import com.chrishaland.tbes.view.SwingConsole;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.client.coprocessor.AggregationClient;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.logging.Level;

public class HBaseConnection {
    private static HBaseAdmin admin = null; //Do not use directly
    private static Configuration config = null;
    private static Configuration rowConfig = null;

    static {
        config = HBaseConfiguration.createConfiguration();
        rowConfig = HBaseConfiguration.createConfiguration(600000, 1000);
    }

    private static HBaseAdmin getHBaseAdmin() {
        if (admin == null) {
            try {
                admin = new HBaseAdmin(config);
            } catch (MasterNotRunningException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            } catch (ZooKeeperConnectionException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }
        }
        return admin;
    }

    public static void createTable(String tableName, String[] families) {
        try {
            HBaseAdmin admin = getHBaseAdmin();

            if (admin.tableExists(Bytes.toBytes(tableName))) {
                SwingConsole.getInstance().log(Level.INFO, "The HBase table " + tableName + " already exists.");
            } else {
                HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
                for (String family : families) {
                    tableDescriptor.addFamily(new HColumnDescriptor(family));
                }

                admin.createTable(tableDescriptor);
                SwingConsole.getInstance().log(Level.INFO, "Created HBase table \"" + tableName + "\" successfully.");
            }
        } catch (MasterNotRunningException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (ZooKeeperConnectionException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public static void removeTable(String tableName) {
        try {
            HBaseAdmin admin = getHBaseAdmin();

            if (admin.tableExists(tableName)) {
                if (admin.isTableEnabled(tableName)) {
                    admin.disableTable(tableName);
                }

                admin.deleteTable(tableName);

                SwingConsole.getInstance().log(Level.INFO, "Removed HBase table \"" + tableName + "\" successfully.");
            } else {
                SwingConsole.getInstance().log(Level.INFO, "The table \"" + tableName + "\" does not exist in the " +
                        "current HBase.");
            }
        } catch (MasterNotRunningException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (ZooKeeperConnectionException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public static HTable getTable(String tableName) {
        try {
            return new HTable(config, tableName);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return null;
    }

    public static long count(String tableName, String family, String qualifier, String value) {
        AggregationClient client = new AggregationClient(rowConfig);

        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(family));

        SingleColumnValueFilter filter = new SingleColumnValueFilter(Bytes.toBytes(family), Bytes.toBytes(qualifier),
                CompareFilter.CompareOp.EQUAL, Bytes.toBytes(value));
        scan.setFilter(filter);

        long rowCount = -1;

        try {
            rowCount = client.rowCount(Bytes.toBytes(tableName), null, scan);

        } catch (Throwable throwable) {
            SwingConsole.getInstance().log(Level.SEVERE, throwable.getStackTrace());
        }

        return rowCount;
    }

    public static void put(HTable table, byte[] rowKey, String family, String qualifier, byte[] value) {
        try {
            Put put = new Put(rowKey);
            put.add(Bytes.toBytes(family), Bytes.toBytes(qualifier), value);

            table.put(put);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public static void put(HTable table, byte[] rowKey, byte[][] families, byte[][] qualifiers, byte[][] values) {
        if (families.length != qualifiers.length || qualifiers.length != values.length) {
            SwingConsole.getInstance().log(Level.INFO, "INFO: Number of families, qualifiers and values differ. " +
                    "Make sure there the byte arrays are set so that each value to be put, has a family and qualifier " +
                    "defined in respect to its i-th place in the array.");
            return;
        }

        try {
            Put put = new Put(rowKey);

            for (int i = 0; i < values.length; i++) {
                put.add(families[i], qualifiers[i], values[i]);
            }

            table.put(put);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    public static Result get(HTable table, String rowKey) {
        Result result = null;

        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            result = table.get(get);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return result;
    }

    public static ResultScanner scan(HTable table, FilterList filterList) {
        try {
            Scan scan = new Scan();
            scan.setFilter(filterList);
            ResultScanner resultScanner = table.getScanner(scan);

            return resultScanner;
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return null;
    }

    public static boolean rowExists(HTable table, String rowKey) {
        boolean rowExists = false;

        try {
            Get get = new Get(Bytes.toBytes(rowKey));

            rowExists = table.exists(get);
        } catch (IOException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return rowExists;
    }

    public static void printResult(Result result) {
        for (KeyValue keyValue : result.raw()) {
            System.out.print(new String(keyValue.getRow()) + " ");
            System.out.print(new String(keyValue.getFamily()) + ":");
            System.out.print(new String(keyValue.getQualifier()) + " ");
            System.out.print(keyValue.getTimestamp() + " ");
            System.out.println(new String(keyValue.getValue()));
        }
    }
}

package com.chrishaland.tbes.controller.twitter;

import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.publisher.Publisher;
import com.chrishaland.tbes.model.twitter.TwitterConnection;
import com.chrishaland.tbes.model.utils.channel.Channel;
import com.chrishaland.tbes.model.utils.json.JSONReader;
import com.chrishaland.tbes.view.SwingConsole;
import com.chrishaland.tbes.view.listeners.CommandEvent;
import com.chrishaland.tbes.view.listeners.CommandListener;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import twitter4j.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;

public class TweetHandler implements StatusListener, CommandListener {
    private static final String JSON_FILE_PATH = "data/json/filter.json";

    private final StatusListener[] STATUS_LISTENERS = {this};

    private Channel<Document> documentChannel;
    private TwitterConnection twitterConnection;

    public TweetHandler(Channel<Document> documentChannel) {
        this.documentChannel = documentChannel;

        JSONObject jsonFilter = JSONReader.readJSONObject(JSON_FILE_PATH);
        String[] trackers = createStringArray((JSONArray) jsonFilter.get("trackers"));
        String[] languages = createStringArray((JSONArray) jsonFilter.get("languages"));

        SwingConsole.getInstance().addCommandListener(this);

        twitterConnection = new TwitterConnection(STATUS_LISTENERS, languages, trackers);
        twitterConnection.start();
    }

    private String[] createStringArray(JSONArray jsonArray) {
        ArrayList<String> sList = new ArrayList<String>();
        Iterator<Object> i = jsonArray.iterator();
        while(i.hasNext()) {
            sList.add((String) i.next());
        }

        return sList.toArray(new String[sList.size()]);
    }

    public TwitterConnection getTwitterConnection() {
        return twitterConnection;
    }

    @Override
    public void onStatus(Status status) {
        try {
            User user = status.getUser();

            Publisher publisher = new Publisher(user.getId(), user.getName(), user.getScreenName(), user.getURL(),
                    user.getStatusesCount(), user.getFollowersCount());

            Document document = new Document(status.getId(), status.getText(), status.getCreatedAt(),
                    status.getRetweetCount(), status.getFavoriteCount(), publisher);

            documentChannel.put(document);
        } catch (IllegalArgumentException e) {
            //Happens frequently (if any of the wanted values are 'null').
        } catch (InterruptedException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }

    @Override
    public void onCommandReceived(CommandEvent e) {
        switch (e.getCommand()) {
            case CommandEvent.PAUSE:
                SwingConsole.getInstance().log(Level.INFO, "The sampling process is paused.");
                twitterConnection.stop();
                break;
            case CommandEvent.RESUME:
                SwingConsole.getInstance().log(Level.INFO, "The sampling process is resuming.");
                twitterConnection.start();
            default:
                break;
        }
    }

    @Override
    public void onException(Exception e) {
        SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
    }

    @Override
    public void onScrubGeo(long x, long y) {
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
    }

    @Override
    public void onTrackLimitationNotice(int trackLimitationNotice) {
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
    }
}

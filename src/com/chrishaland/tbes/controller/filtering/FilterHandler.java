package com.chrishaland.tbes.controller.filtering;

import com.chrishaland.tbes.model.data.Construct;
import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.relation.Relation;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.filters.MachineLearningFilter;
import com.chrishaland.tbes.model.filters.RegexFilter;
import com.chrishaland.tbes.model.utils.channel.Channel;
import com.chrishaland.tbes.view.SwingConsole;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;

public class FilterHandler implements Runnable {
    private long documentsCountTotal, documentsPassedFilter1, documentsPassedFilter2, relationsCreatedForFilter1,
            relationsCreatedForFilter2;

    private Thread thread;
    private Semaphore semaphore;
    private Channel<Document> documentChannel;
    private Channel<Construct> constructChannel;

    private RegexFilter regexFilter;
    private MachineLearningFilter machineLearningFilter;

    public FilterHandler(Channel<Document> documentChannel, Channel<Construct> constructChannel,
                         TopicMap topicMap) {
        resetCounters();

        this.documentChannel = documentChannel;
        this.constructChannel = constructChannel;

        regexFilter = new RegexFilter(topicMap);
        machineLearningFilter = new MachineLearningFilter(topicMap);

        semaphore = new Semaphore(0);
        new ModelTraining(semaphore, machineLearningFilter, this);

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                semaphore.release();

                Document document = documentChannel.take();
                Relation[] regexRelations = regexFilter.filter(document);

                documentsCountTotal += 1;
                if (regexRelations != null) {
                    documentsPassedFilter1 += 1;
                    relationsCreatedForFilter1 += regexRelations.length;

                    if(machineLearningFilter.isClassifierActive()) {
                        Relation[] machineLearningRelations = machineLearningFilter.filter(regexRelations);

                        if (machineLearningRelations != null) {
                            documentsPassedFilter2 += 1;
                            relationsCreatedForFilter2 += machineLearningRelations.length;
                            constructChannel.put(new Construct(document, document.getPublisher(), machineLearningRelations));
                        }
                    } else {
                        constructChannel.put(new Construct(document, document.getPublisher(), regexRelations));
                    }
                }

                semaphore.acquire();
            } catch (InterruptedException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }
        }
    }

    public long getDocumentsCountTotal() {
        return documentsCountTotal;
    }

    public long getDocumentsPassedFilter1() {
        return documentsPassedFilter1;
    }

    public long getDocumentsPassedFilter2() {
        return documentsPassedFilter2;
    }

    public long getRelationsCreatedForFilter1() {
        return relationsCreatedForFilter1;
    }

    public long getRelationsCreatedForFilter2() {
        return relationsCreatedForFilter2;
    }

    public void resetCounters() {
        documentsCountTotal = 0;
        documentsPassedFilter1 = 0;
        documentsPassedFilter2 = 0;
        relationsCreatedForFilter1 = 0;
        relationsCreatedForFilter2 = 0;
    }
}

package com.chrishaland.tbes.controller.filtering;

import com.chrishaland.tbes.model.filters.MachineLearningFilter;
import com.chrishaland.tbes.model.hbase.tables.RelationTable;
import com.chrishaland.tbes.model.mysql.MySQLConnection;
import com.chrishaland.tbes.view.SwingConsole;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;

class ModelTraining implements Runnable {
    private static final long RELATIONS_NEEDED_FOR_MACHINE_LEARNING_FILTER = 500;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");

    private Thread thread;
    private Semaphore semaphore;
    private Calendar nextTrainingTime;
    private FilterHandler filterHandler;
    private MachineLearningFilter machineLearningFilter;

    public ModelTraining(Semaphore semaphore, MachineLearningFilter machineLearningFilter, FilterHandler filterHandler) {
        this.semaphore = semaphore;
        this.filterHandler = filterHandler;
        this.machineLearningFilter = machineLearningFilter;

        nextTrainingTime = createCalendarTime();

        String showTableQuery = "SHOW TABLES LIKE 'data_log'";
        MySQLConnection mySQLConnection = new MySQLConnection();

        try {
            MySQLConnection.Query showTableQueryResult = mySQLConnection.executeQuery(showTableQuery);

            if (!showTableQueryResult.getResultSet().first()) {
                String createTableQuery = "CREATE TABLE data_log (" +
                        "uid INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                        "documents_count_total BIGINT NOT NULL," +
                        "documents_passed_filter_1 BIGINT NOT NULL," +
                        "relations_created_by_filter_1 BIGINT NOT NULL," +
                        "documents_passed_filter_2 BIGINT," +
                        "relations_created_by_filter_2 BIGINT," +
                        "model_instances_count BIGINT, " +
                        "timestamp TIMESTAMP NOT NULL)";
                SwingConsole.getInstance().log(Level.INFO, "Creating table 'data_log' on MySQL server.");
                MySQLConnection.Update createTableResult = mySQLConnection.executeUpdate(createTableQuery);
                createTableResult.close();
            }
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } finally {
            if (mySQLConnection != null) {
                mySQLConnection.close();
            }
        }

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                long difference = nextTrainingTime.getTimeInMillis() - System.currentTimeMillis();

                SwingConsole.getInstance().log(Level.INFO, "Time until retraining the model: " + difference + "ms.");
                Thread.sleep((difference < 1000 ? 1000 : difference) / 2);

                semaphore.acquire();

                Calendar currentTime = Calendar.getInstance();
                currentTime.setTime(new Date(System.currentTimeMillis()));

                if (nextTrainingTime.before(currentTime)) {
                    SwingConsole.getInstance().executePauseCommand();
                    String insertDataLogQuery;

                    if (!machineLearningFilter.isClassifierActive()) {
                        insertDataLogQuery = "INSERT INTO data_log (documents_count_total, " +
                                "documents_passed_filter_1, relations_created_by_filter_1, timestamp) VALUES (" +
                                filterHandler.getDocumentsCountTotal() + ", " +
                                filterHandler.getDocumentsPassedFilter1() + ", " +
                                filterHandler.getRelationsCreatedForFilter1() + ", '" +
                                getDateFormat() + "')";
                    } else {
                        insertDataLogQuery = "INSERT INTO data_log (documents_count_total, documents_passed_filter_1, " +
                                "relations_created_by_filter_1, documents_passed_filter_2, " +
                                "relations_created_by_filter_2, model_instances_count, timestamp) VALUES (" +
                                filterHandler.getDocumentsCountTotal() + ", " +
                                filterHandler.getDocumentsPassedFilter1() + ", " +
                                filterHandler.getRelationsCreatedForFilter1() + ", " +
                                filterHandler.getDocumentsPassedFilter2() + ", " +
                                filterHandler.getRelationsCreatedForFilter2() + ", " +
                                machineLearningFilter.getCurrentModelInstancesCount() + ", '" +
                                getDateFormat() + "')";
                    }

                    MySQLConnection mySQLConnection = new MySQLConnection();
                    MySQLConnection.Update updateResult = mySQLConnection.executeUpdate(insertDataLogQuery);
                    updateResult.close();
                    mySQLConnection.close();

                    if (reachedRelationsNeededForMachineLearningFilterLimit()) {
                        SwingConsole.getInstance().log(Level.INFO, "Retraining the machine learning model.");
                        machineLearningFilter.rebuild();
                    }

                    filterHandler.resetCounters();
                    nextTrainingTime.add(Calendar.DATE, 1);

                    SwingConsole.getInstance().executeResumeCommand();
                }

                semaphore.release();
            } catch (InterruptedException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }

        }
    }

    private boolean reachedRelationsNeededForMachineLearningFilterLimit() {
        return RelationTable.countFeedback() >= RELATIONS_NEEDED_FOR_MACHINE_LEARNING_FILTER;
    }

    private Calendar createCalendarTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));

        calendar.add(Calendar.HOUR, -calendar.get(Calendar.HOUR_OF_DAY));
        calendar.add(Calendar.MINUTE, -calendar.get(Calendar.MINUTE));
        calendar.add(Calendar.SECOND, -calendar.get(Calendar.SECOND));
        calendar.add(Calendar.MILLISECOND, -calendar.get(Calendar.MILLISECOND));

        calendar.add(Calendar.DATE, 1);
        calendar.add(Calendar.HOUR, 12);

        return calendar;
    }

    private String getDateFormat() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(Calendar.DATE, -1);
        return DATE_FORMAT.format(calendar.getTime());
    }
}

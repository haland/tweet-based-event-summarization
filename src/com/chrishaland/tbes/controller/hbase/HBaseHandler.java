package com.chrishaland.tbes.controller.hbase;

import com.chrishaland.tbes.model.data.Construct;
import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.publisher.Publisher;
import com.chrishaland.tbes.model.data.relation.Relation;
import com.chrishaland.tbes.model.hbase.tables.DocumentTable;
import com.chrishaland.tbes.model.hbase.tables.PublisherTable;
import com.chrishaland.tbes.model.hbase.tables.RelationTable;
import com.chrishaland.tbes.model.utils.channel.Channel;
import com.chrishaland.tbes.view.SwingConsole;

import java.util.logging.Level;

public class HBaseHandler implements Runnable {
    private Thread thread;
    private Channel<Construct> constructChannel;

    public HBaseHandler(Channel<Construct> constructChannel) {
        this.constructChannel = constructChannel;

        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Construct construct = constructChannel.take();
                Relation[] relations = construct.getRelations();

                if (relations != null) {

                    Publisher p = construct.getPublisher();
                    PublisherTable.insert(p.getId(), p.getName(), p.getUsername(), p.getUrl(), p.getTweetCount(),
                            p.getFollowersCount());

                    Document d = construct.getDocument();
                    DocumentTable.insert(d.getId(), d.getText(), d.getCreatedAt(), d.getRetweetCount(),
                            d.getFavoriteCount(), p.getId(), p.getUsername());

                    for (Relation relation : relations) {
                        RelationTable.insert(relation.getId(), relation.getTopic().getId(),
                                relation.getDocument().getId(), relation.hasFeedback(), relation.isFeedbackPositive());
                    }
                }
            } catch(InterruptedException e) {
                SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
            }
        }
    }

    public static void createHBaseTables() {
        RelationTable.create();
        DocumentTable.create();
        PublisherTable.create();
    }

    public static void removeHBaseTables() {
        RelationTable.remove();
        DocumentTable.remove();
        PublisherTable.remove();
    }
}

package com.chrishaland.tbes.controller.topic;

import com.chrishaland.tbes.model.data.topic.Topic;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.mysql.MySQLConnection;
import com.chrishaland.tbes.view.SwingConsole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;

public class TopicHandler extends ArrayList<Topic> {
    private static TopicMap topicMap;

    static {
        topicMap = new TopicMap();
    }

    public TopicHandler() {
        writeTopicsToMySQL();
    }

    private void writeTopicsToMySQL() {
        String showTableQuery = "SHOW TABLES LIKE 'topic'";
        MySQLConnection mySQLConnection = new MySQLConnection();

        try {
            MySQLConnection.Query showTableQueryResult = mySQLConnection.executeQuery(showTableQuery);

            if (!showTableQueryResult.getResultSet().first()) {
                SwingConsole.getInstance().log(Level.INFO, "Creating table 'topic' on MySQL server.");
                String createTableQuery = "CREATE TABLE topic (" +
                        "uid INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                        "id VARCHAR(50) NOT NULL," +
                        "name VARCHAR(50) NOT NULL," +
                        "regex VARCHAR(255) NOT NULL," +
                        "parent_id VARCHAR(50)" +
                        ")";
                MySQLConnection.Update createTableResult = mySQLConnection.executeUpdate(createTableQuery);
                createTableResult.close();
            }

            Iterator<Topic> i = topicMap.values().iterator();
            while(i.hasNext()) {
                writeTopicToDatabase(i.next(), null, mySQLConnection);
            }


            showTableQueryResult.close();
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        } finally {
            if (mySQLConnection != null) {
                mySQLConnection.close();
            }
        }
    }

    private void writeTopicToDatabase(Topic topic, Topic parent, MySQLConnection mySQLConnection) {
        if (!topicExists(topic, mySQLConnection)) {
            String insertTopicQuery;

            if (parent == null) {
                insertTopicQuery = "INSERT INTO topic (id, name, regex) VALUES ('" + topic.getId() + "', '" +
                        topic.getName() + "', '" + topic.getRegex() + "')";
            } else {
                insertTopicQuery = "INSERT INTO topic (id, name, regex, parent_id) VALUES ('" + topic.getId() +
                        "', '" + topic.getName() + "', '" + topic.getRegex() + "', '" + topic.getParent().getId() +
                        "')";
            }

            MySQLConnection.Update updateResult = mySQLConnection.executeUpdate(insertTopicQuery);
            updateResult.close();
        }

        ArrayList<Topic> children = topic.getChildren();
        if (!children.isEmpty()) {
            for (Topic child : children) {
                writeTopicToDatabase(child, topic, mySQLConnection);
            }
        }
    }

    private boolean topicExists(Topic topic, MySQLConnection mySQLConnection) {
        String id = null;

        try {
            String selectTopicQuery = "SELECT id FROM topic WHERE id = '" + topic.getId() + "' LIMIT 1";
            MySQLConnection.Query queryResult = mySQLConnection.executeQuery(selectTopicQuery);
            ResultSet selectTopicResultSet = queryResult.getResultSet();

            if (selectTopicResultSet.first()) {
                selectTopicResultSet.absolute(1);
                id = selectTopicResultSet.getString(1);
            }

            queryResult.close();
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }

        return id != null;
    }

    public TopicMap getTopicMap() {
        return topicMap;
    }

    public static void dropTopicTable() {
        MySQLConnection mySQLConnection = new MySQLConnection();
        String dropQuery = "DROP TABLE IF EXISTS topic";
        mySQLConnection.executeUpdate(dropQuery);
        mySQLConnection.close();

        SwingConsole.getInstance().log(Level.INFO, "Dropped table 'topic' from MySQL server.");
    }
}

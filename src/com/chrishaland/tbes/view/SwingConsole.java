package com.chrishaland.tbes.view;

import com.chrishaland.tbes.view.listeners.CommandEvent;
import com.chrishaland.tbes.view.listeners.CommandListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;

public class SwingConsole extends JFrame implements ActionListener {
    private static SwingConsole instance;
    private static final int ENTRIES_LIMIT = 50;

    private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String output;
    private int currentIndex;
    private String[] currentEntries;
    private Vector<CommandListener> listeners;

    private JTextArea textArea;
    private JScrollPane scrollPane;
    private JPanel panel, cmdPanel;
    private JTextField cmdTextField;

    private SwingConsole(int entriesLimit) {
        super("Console");

        currentIndex = 0;
        currentEntries = new String[entriesLimit];
        listeners = new Vector<CommandListener>();

        textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        textArea.setWrapStyleWord(true);

        scrollPane = new JScrollPane(textArea);

        cmdTextField = new JTextField();
        cmdTextField.addActionListener(this);

        cmdPanel = new JPanel(new BorderLayout());
        cmdPanel.add(cmdTextField, BorderLayout.CENTER);

        panel = new JPanel(new BorderLayout());
        panel.add(scrollPane, BorderLayout.CENTER);
        panel.add(cmdPanel, BorderLayout.SOUTH);

        setVisible(true);
        setSize(564, 376);

        setPosition();
        setResizable(false);
        setContentPane(panel);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    public synchronized void log(Level level, String entry) {
        currentIndex = (currentIndex + 1) % currentEntries.length;
        currentEntries[currentIndex] = "Level: " + level.getName() + ", Time: " +
                DATE_FORMATTER.format(new Date(System.currentTimeMillis())) + ", Entry: " + entry;
        updateConsole();
    }

    public synchronized void log(Level level, StackTraceElement[] stackTraceElements) {
        String stackTrace = "";
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            stackTrace = stackTrace + stackTraceElement.getClassName() + "\n" +
                    "   at " + stackTraceElement.getClassName() + "." +
                    stackTraceElement.getMethodName() + "(" + stackTraceElement.getFileName() +
                    ":" + stackTraceElement.getLineNumber() + ")\n";
        }

        log(level, stackTrace);
    }

    public void executePauseCommand() {
        cmdTextField.setText("pause");
        actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
    }

    public void executeResumeCommand() {
        cmdTextField.setText("resume");
        actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int command = -1;
        String commandText = cmdTextField.getText();

        if (commandText.equals("exit")) {
            command = CommandEvent.EXIT;
        } else if (commandText.equals("pause")) {
            command = CommandEvent.PAUSE;
        } else if (commandText.equals("resume")) {
            command = CommandEvent.RESUME;
        }

        cmdTextField.setText("");
        notifyListeners(new CommandEvent(command));
    }

    private void notifyListeners(CommandEvent e) {
        Iterator<CommandListener> i = listeners.iterator();
        while (i.hasNext())
            i.next().onCommandReceived(e);
    }

    public void addCommandListener(CommandListener l) {
        if (!listeners.contains(l))
            listeners.add(l);
    }

    public void removeCommandListener(CommandListener l) {
        if (listeners.contains(l))
            listeners.remove(l);
    }

    private void updateConsole() {
        output = "";

        int index = currentIndex;
        do {
            index = (index + 1) % currentEntries.length;

            if (currentEntries[index] != null) {
                output = (output.equals("") ? "" : output + "\n") + currentEntries[index];
            }
        } while (index != currentIndex);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                textArea.setText(output);
            }
        });
    }

    private void setPosition() {
        int xOrg = Toolkit.getDefaultToolkit().getScreenSize().width / 2;
        int yOrg = Toolkit.getDefaultToolkit().getScreenSize().height / 2;

        int xPos = xOrg - getWidth() / 2;
        int yPos = yOrg - getHeight() / 2;

        if (xPos < 0)
            xPos = 0;
        if (yPos < 0)
            yPos = 0;

        setLocation(xPos, yPos);
    }

    public static SwingConsole getInstance() {
        if (instance == null) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (UnsupportedLookAndFeelException e) {
                e.printStackTrace();
            }

            instance = new SwingConsole(ENTRIES_LIMIT);
        }

        return instance;
    }
}

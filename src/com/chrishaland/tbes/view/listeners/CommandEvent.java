package com.chrishaland.tbes.view.listeners;

public class CommandEvent {
    public static final int NOT_A_COMMAND = -1;

    public static final int EXIT = 0x00;
    public static final int PAUSE = 0x01;
    public static final int RESUME = 0x02;

    private int command;

    public CommandEvent(int command) {
        this.command = command;
    }

    public int getCommand() {
        return command;
    }
}

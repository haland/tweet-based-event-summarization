package com.chrishaland.tbes.view.listeners;

public interface CommandListener {
    public abstract void onCommandReceived(CommandEvent e);
}

package com.chrishaland.tbes.builds.evaluation.micro;

import com.chrishaland.tbes.builds.evaluation.Evaluating;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SplitMicroEvaluation {
    private static final String[] CLASSIFIER_NAMES = { "rf", "nb", "id" };
    private static final Classifier[] CLASSIFIER_OBJECTS = { new RandomForest(), new NaiveBayes(), new J48() };

    private static final String[] AMBIGUOUS = { "suits", "vikings", "supernatural", "firefly" };
    private static final String[] UNAMBIGUOUS = { "agents_of_shield", "big_bang_theory_the", "doctor_who",
            "hannibal", "whose_line_is_it_anyway" };

    private static final String[] TOPIC_NAMES = {"Ambiguous", "Unambiguous"};
    private static final String[][] TOPIC_OBJECTS = {AMBIGUOUS, UNAMBIGUOUS};

    private static Calendar ct;

    public static void main(String[] args) {
        for (int h = 0; h < TOPIC_OBJECTS.length; h++) {
            ArrayList<Double> posList = new ArrayList<Double>();
            ArrayList<Double> negList = new ArrayList<Double>();
            ArrayList<Integer> instancesList = new ArrayList<Integer>();

            for (int i = 0; i < CLASSIFIER_OBJECTS.length; i++) {

                File[] models = new File("data/weka/testing/model/").listFiles();
                ct = Evaluating.createDates(models);

                ArrayList<Double> precisionList = new ArrayList<Double>();

                for (int j = 0; j < models.length - 1; j++) {
                    Instances trainingModel = Evaluating.readInstances("data/weka/testing/model/" +
                            Evaluating.printDate(ct) + "-model.arff", "data/weka/testing/relations/" +
                            Evaluating.printDate(ct) + "-relations.arff", TOPIC_OBJECTS[h]);
                    Instances testData = Evaluating.readInstances("data/weka/complete/model.arff",
                            "data/weka/complete/relations.arff", TOPIC_OBJECTS[h]);

                    for (int k = trainingModel.numInstances()-1; k >= 0; k--) {
                        testData.delete(k);
                    }

                    int positive = 0;
                    double[] feedbacks = testData.attributeToDoubleArray(4);
                    for (double feedback : feedbacks) {
                        if(feedback == (double) 0) {
                            positive++;
                        }
                    }

                    double precision = Evaluating.evaluate(CLASSIFIER_OBJECTS[i], trainingModel, testData);

                    if (i == 0) {
                        instancesList.add(testData.numInstances());
                        posList.add((positive * 1.0) / testData.numInstances());
                        negList.add((testData.numInstances() - positive * 1.0) / testData.numInstances());
                    }

                    precisionList.add(precision);

                    ct.add(Calendar.DATE, 1);
                }

                System.out.print(CLASSIFIER_NAMES[i] + TOPIC_NAMES[h] + " = [");

                boolean isFirst = true;
                ArrayList<Integer> invalidValuesIndices = new ArrayList<Integer>();
                for (int j = 0; j < precisionList.size(); j++) {
                    if (precisionList.get(j) == 0 || Double.isNaN(precisionList.get(j)) ||
                            Double.isInfinite(precisionList.get(j))) {
                        invalidValuesIndices.add(j);
                    } else if (isFirst) {
                        isFirst = false;
                        System.out.print(Evaluating.printDouble(precisionList.get(j)));
                    } else {
                        System.out.print(", " + Evaluating.printDouble(precisionList.get(j)));
                    }
                }

                System.out.println("];");

                if (i == 0) {
                    for (int j = invalidValuesIndices.size() - 1; j >= 0; j--) {
                        posList.remove((int) invalidValuesIndices.get(j));
                        negList.remove((int) invalidValuesIndices.get(j));
                        instancesList.remove((int) invalidValuesIndices.get(j));
                    }
                }
            }

            System.out.print("in" + TOPIC_NAMES[h] + " = [");

            for (int j = 0; j < instancesList.size(); j++) {
                if (j == 0) {
                    System.out.print(instancesList.get(j));
                } else {
                    System.out.print(", " + instancesList.get(j));
                }
            }

            System.out.println("];");
            System.out.print("po" + TOPIC_NAMES[h] + " = [");

            for (int j = 0; j < posList.size(); j++) {
                if (j == 0) {
                    System.out.print(Evaluating.printDouble(posList.get(j)));
                } else {
                    System.out.print(", " + Evaluating.printDouble(posList.get(j)));
                }
            }

            System.out.println("];");
            System.out.print("ne" + TOPIC_NAMES[h] + " = [");

            for (int j = 0; j < posList.size(); j++) {
                if (j == 0) {
                    System.out.print(Evaluating.printDouble(negList.get(j)));
                } else {
                    System.out.print(", " + Evaluating.printDouble(negList.get(j)));
                }
            }

            System.out.println("];");
            System.out.println();
        }
    }
}
package com.chrishaland.tbes.builds.evaluation.micro;

import com.chrishaland.tbes.builds.evaluation.Evaluating;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class SingleMicroEvaluation {
    private static final String[] CLASSIFIER_NAMES = { "rf", "nb", "id" };
    private static final Classifier[] CLASSIFIER_OBJECTS = { new RandomForest(), new NaiveBayes(), new J48() };

    private static Calendar ct;

    public static void main(String[] args) {
        ArrayList<Double> posList = new ArrayList<Double>();
        ArrayList<Double> negList = new ArrayList<Double>();
        ArrayList<Integer> instancesList = new ArrayList<Integer>();

        for (int i = 0; i < CLASSIFIER_OBJECTS.length; i++) {
            File[] models = new File("data/weka/testing/model/").listFiles();
            ct = Evaluating.createDates(models);

            ArrayList<Double> precisionList = new ArrayList<Double>();

            for (int j = 0; j < models.length - 1; j++) {
                try {
                    ConverterUtils.DataSource trainingSource = new ConverterUtils.DataSource(
                            "data/weka/testing/model/" + Evaluating.printDate(ct) + "-model.arff");
                    Instances trainingModel = trainingSource.getDataSet();

                    if (trainingModel.classIndex() == -1) {
                        trainingModel.setClassIndex(trainingModel.numAttributes() - 1);
                    }

                    ConverterUtils.DataSource testingSource =
                            new ConverterUtils.DataSource("data/weka/complete/model.arff");
                    Instances testData = testingSource.getDataSet();

                    if (testData.classIndex() == -1)
                        testData.setClassIndex(testData.numAttributes() - 1);

                    for (int h = trainingModel.numInstances()-1; h >= 0; h--) {
                        testData.delete(h);
                    }

                    int positive = 0;
                    double[] feedbacks = testData.attributeToDoubleArray(4);
                    for (double feedback : feedbacks) {
                        if(feedback == (double) 0) {
                            positive++;
                        }
                    }

                    double precision = Evaluating.evaluate(CLASSIFIER_OBJECTS[i], trainingModel, testData);

                    if (i == 0) {
                        instancesList.add(testData.numInstances());
                        posList.add((positive * 1.0) / testData.numInstances());
                        negList.add((testData.numInstances() - positive * 1.0) / testData.numInstances());
                    }

                    precisionList.add(precision);

                    ct.add(Calendar.DATE, 1);
                } catch (Exception e) {

                }
            }

            System.out.print(CLASSIFIER_NAMES[i] + "Grouped = [");

            boolean isFirst = true;
            ArrayList<Integer> invalidValuesIndices = new ArrayList<Integer>();
            for (int j = 0; j < precisionList.size(); j++) {
                if (precisionList.get(j) == 0 || Double.isNaN(precisionList.get(j)) ||
                        Double.isInfinite(precisionList.get(j))) {
                    invalidValuesIndices.add(j);
                } else if (isFirst) {
                    isFirst = false;
                    System.out.print(Evaluating.printDouble(precisionList.get(j)));
                } else {
                    System.out.print(", " + Evaluating.printDouble(precisionList.get(j)));
                }
            }

            System.out.println("];");

            if (i == 0) {
                for (int j = invalidValuesIndices.size() - 1; j >= 0; j--) {
                    posList.remove((int) invalidValuesIndices.get(j));
                    negList.remove((int) invalidValuesIndices.get(j));
                    instancesList.remove((int) invalidValuesIndices.get(j));
                }
            }
        }


        System.out.print("inGrouped = [");

        for (int j = 0; j < instancesList.size(); j++) {
            if (j == 0) {
                System.out.print(instancesList.get(j));
            } else {
                System.out.print(", " + instancesList.get(j));
            }
        }

        System.out.println("];");
        System.out.print("poGrouped = [");

        for (int j = 0; j < posList.size(); j++) {
            if (j == 0) {
                System.out.print(Evaluating.printDouble(posList.get(j)));
            } else {
                System.out.print(", " + Evaluating.printDouble(posList.get(j)));
            }
        }

        System.out.println("];");
        System.out.print("neGrouped = [");

        for (int j = 0; j < posList.size(); j++) {
            if (j == 0) {
                System.out.print(Evaluating.printDouble(negList.get(j)));
            } else {
                System.out.print(", " + Evaluating.printDouble(negList.get(j)));
            }
        }

        System.out.println("];");
        System.out.println();
    }
}

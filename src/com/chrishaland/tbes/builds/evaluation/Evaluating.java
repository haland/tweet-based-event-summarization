package com.chrishaland.tbes.builds.evaluation;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.*;

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Evaluating {
    private static final SimpleDateFormat DATE_FORMAT;
    private static final DecimalFormat DECIMAL_FORMAT;

    private static final FastVector attributeList;

    private static final FastVector relationClassifications;
    private static final Attribute relationAttribute;

    private static final Attribute childrenTrackersAttribute = new Attribute("children_trackers");
    private static final Attribute negativeTrackersAttribute = new Attribute("negative_trackers");
    private static final Attribute positiveTrackersAttribute = new Attribute("positive_trackers");
    private static final Attribute positiveTrackersAsHashtagsAttribute = new Attribute("positive_trackers_as_hashtags");

    static {
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
        DECIMAL_FORMAT = new DecimalFormat("0.000");

        DecimalFormatSymbols symbols = DECIMAL_FORMAT.getDecimalFormatSymbols();
        symbols.setDecimalSeparator('.');

        DECIMAL_FORMAT.setDecimalFormatSymbols(symbols);

        relationClassifications = new FastVector(2);
        relationClassifications.addElement("positive");
        relationClassifications.addElement("negative");

        relationAttribute = new Attribute("relation", relationClassifications);

        attributeList = new FastVector(5);
        attributeList.addElement(positiveTrackersAttribute);
        attributeList.addElement(positiveTrackersAsHashtagsAttribute);
        attributeList.addElement(negativeTrackersAttribute);
        attributeList.addElement(childrenTrackersAttribute);
        attributeList.addElement(relationAttribute);
    }

    public static double evaluate(Classifier classifier, Instances trainingModel, Instances testData) {
        try {
            classifier.buildClassifier(trainingModel);

            Evaluation evaluation = new Evaluation(trainingModel);
            evaluation.evaluateModel(classifier, testData);

            return evaluation.precision(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static Instances readInstances(String modelPath, String relationPath, String prefix) {
        try {
            Instances instances = new Instances("documents", attributeList, 0);

            BufferedReader bufferedModelReader = new BufferedReader(new FileReader(modelPath));
            BufferedReader bufferedRelationReader = new BufferedReader(new FileReader(relationPath));

            int definitionCounter = 0;
            while(definitionCounter < 8) {
                definitionCounter++;
                bufferedModelReader.readLine();
            }

            definitionCounter = 0;
            while(definitionCounter < 5) {
                definitionCounter++;
                bufferedRelationReader.readLine();
            }

            String modelLine = bufferedModelReader.readLine();
            String relationLine = bufferedRelationReader.readLine();

            while(modelLine != null && !modelLine.isEmpty() && relationLine != null && !relationLine.isEmpty()) {
                Pattern pattern = Pattern.compile(prefix);
                Matcher matcher = pattern.matcher(relationLine);

                if (matcher.find()) {
                    String[] values = modelLine.split(", ");
                    Instance instance = new SparseInstance(5);
                    instance.setValue(positiveTrackersAttribute, Integer.valueOf(values[0]));
                    instance.setValue(positiveTrackersAsHashtagsAttribute, Integer.valueOf(values[1]));
                    instance.setValue(negativeTrackersAttribute, Integer.valueOf(values[2]));
                    instance.setValue(childrenTrackersAttribute, Integer.valueOf(values[3]));
                    instance.setValue(relationAttribute, values[4]);

                    instances.add(instance);
                }

                modelLine = bufferedModelReader.readLine();
                relationLine = bufferedRelationReader.readLine();
            }

            if (instances.classIndex() == -1)
                instances.setClassIndex(instances.numAttributes()-1);

            bufferedModelReader.close();
            bufferedRelationReader.close();

            return instances;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Instances readInstances(String modelPath, String relationPath, String[] prefixes) {
        String regex = "(?i)" + prefixes[0];
        for (int i = 1; i < prefixes.length; i++) {
            regex += "|" + prefixes[i];
        }

        return  readInstances(modelPath, relationPath, regex);
    }

    public static int instanceCountInModel(String relationPath, String prefix) {
        try {
            int instances = 0;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(relationPath));

            int definitionCounter = 0;
            while(definitionCounter < 5) {
                definitionCounter++;
                bufferedReader.readLine();
            }

            String instanceLine = bufferedReader.readLine();
            while(instanceLine != null && !instanceLine.isEmpty()) {
                if (instanceLine.contains(prefix)) {
                    instances++;
                }

                instanceLine = bufferedReader.readLine();
            }

            return instances;
        } catch (IOException e) {
            return -1;
        }
    }

    public static String printDouble(double d) {
        return DECIMAL_FORMAT.format(d);
    }

    public static String printDate(Calendar calendar) {
        return DATE_FORMAT.format(calendar.getTime());
    }

    public static Calendar createDates(File[] models) {
        String[] splitName = models[0].getName().split("-");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(splitName[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(splitName[1]) - 1);
        calendar.set(Calendar.DATE, Integer.valueOf(splitName[2]));

        return calendar;
    }
}

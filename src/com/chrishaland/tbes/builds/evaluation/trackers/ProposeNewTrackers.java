package com.chrishaland.tbes.builds.evaluation.trackers;

import com.chrishaland.tbes.model.hbase.HBaseConnection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProposeNewTrackers {
    private static final String[] RELATION_VALUES = { "positive", "negative" };
    private static final String[] TOPIC_VALUES = { "agents_of_shield", "big_bang_theory_the", "doctor_who",
            "firefly", "hannibal", "suits", "supernatural", "vikings", "whose_line_is_it_anyway" };

    private static final SimpleDateFormat DATE_FORMAT;
    private static final DecimalFormat DECIMAL_FORMAT;

    private static final HTable documentTable;

    static {
        documentTable = HBaseConnection.getTable("document");
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

        DECIMAL_FORMAT = new DecimalFormat("0.000");

        DecimalFormatSymbols symbols = DECIMAL_FORMAT.getDecimalFormatSymbols();
        symbols.setDecimalSeparator('.');

        DECIMAL_FORMAT.setDecimalFormatSymbols(symbols);
    }

    public static void main(String[] args) {
        TreeMap<String,Integer>[] wordMaps = new TreeMap[TOPIC_VALUES.length];
        TreeMap<String,Double>[] logLikelihoodMaps = new TreeMap[TOPIC_VALUES.length];

        File[] models = new File("data/weka/models/").listFiles();
        String modelDate = createDate(models[models.length-1]);

        for (int i = 0; i < TOPIC_VALUES.length; i++) {
            TreeMap<String,Integer> wordMap = wordCount(modelDate, TOPIC_VALUES[i], "negative");
            wordMaps[i] = wordMap;
        }

        for (int i = 0; i < wordMaps.length; i++) {
            TreeMap<String,Double> g = new TreeMap<String, Double>();

            TreeMap<String,Integer> c1 = wordMaps[i];
            TreeMap<String,Integer> c2 = new TreeMap<String, Integer>();

            for (int j = 0; j < wordMaps.length; j++) {
                TreeMap<String,Integer> wordMap = wordMaps[j];
                Iterator<String> iKeys = wordMap.keySet().iterator();
                Iterator<Integer> iValues = wordMap.values().iterator();

                while(iKeys.hasNext()) {
                    String key = iKeys.next();
                    int value = iValues.next();

                    if (c2.containsKey(key)) {
                        c2.put(key, c2.get(key) + value);
                    } else {
                        c2.put(key, value);
                    }
                }
            }

            int c = totalWordFrequency(c1);
            int d = totalWordFrequency(c2);

            Iterator<String> iKeys = c1.keySet().iterator();
            Iterator<Integer> iValues = c1.values().iterator();
            while(iKeys.hasNext()) {
                String key = iKeys.next();

                int a = iValues.next();
                int b;

                try {
                    b = c2.get(key);
                } catch (NullPointerException e) {
                    b = 0;
                }

                double e1 = c*(a+b)/(c+d);
                double e2 = d*(a+b)/(c+d);

                double g2 = 2*((a*Math.log(a/e1)) + (b*Math.log(b/e2)));
                if (!Double.isInfinite(g2) && !Double.isNaN(g2))
                    g.put(key, g2);
            }

            logLikelihoodMaps[i] = g;
        }

        for(int i = 0; i < logLikelihoodMaps.length; i++) {
            ValueComparator2 valueComparator = new ValueComparator2(logLikelihoodMaps[i]);
            TreeMap<String,Double> logMap = new TreeMap<String, Double>(valueComparator);
            logMap.putAll(logLikelihoodMaps[i]);
            System.out.println("Log likelihood values for " + TOPIC_VALUES[i] + ":");

            Iterator<String> iKeys = logMap.keySet().iterator();
            Iterator<Double> iValues = logMap.values().iterator();

            while(iKeys.hasNext()) {
                String key = iKeys.next();
                double val = iValues.next();

                System.out.print(key + "||" + DECIMAL_FORMAT.format(val) + "|");
            }

            System.out.println();
        }
    }

    private static int totalWordFrequency(TreeMap<String,Integer> wordMap) {
        int freq = 0;
        Iterator<Integer> i = wordMap.values().iterator();
        while(i.hasNext()) {
            freq += i.next();
        }

        return freq;
    }

    private static TreeMap<String,Integer> wordCount(String modelDate, String topic, String relationValue) {
        try {
            HashMap<String, Integer> wordMap = new HashMap<String, Integer>();

            BufferedReader bufferedModelReader = new BufferedReader(new FileReader("data/weka/models/" + modelDate +
                    "-model.arff"));
            BufferedReader bufferedRelationReader = new BufferedReader(new FileReader("data/weka/relations/" + modelDate +
                    "-relations.arff"));

            int definitionCounter = 0;
            while(definitionCounter < 8) {
                definitionCounter++;
                bufferedModelReader.readLine();
            }

            definitionCounter = 0;
            while(definitionCounter < 5) {
                definitionCounter++;
                bufferedRelationReader.readLine();
            }

            String modelLine = bufferedModelReader.readLine();
            String relationLine = bufferedRelationReader.readLine();

            while(modelLine != null && !modelLine.isEmpty() && relationLine != null && !relationLine.isEmpty()) {
                String[] ids = relationLine.split(", ");
                String[] values = modelLine.split(", ");

                if (ids[1].contains(topic) && values[4].equals(relationValue)) {
                    Get get = new Get(Bytes.toBytes(ids[0]));
                    Result result = documentTable.get(get);

                    String text = new String(result.getValue(Bytes.toBytes("document"), Bytes.toBytes("text")));

                    Pattern pattern = Pattern.compile("\\(?\\b(((ftp|https*)://(www[.])*)|(www[.]))[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]");
                    Matcher matcher = pattern.matcher(text);

                    String noURLText = matcher.replaceAll("");

                    Pattern pattern1 = Pattern.compile("[^\\w\\s]");
                    Matcher matcher1 = pattern1.matcher(noURLText);

                    String noSpecialCharacterText = matcher1.replaceAll("");
                    String[] words = noSpecialCharacterText.split(" ");

                    for (String word : words) {
                        word = word.replace("\n", "");
                        word = word.toLowerCase();
                        if (wordMap.containsKey(word)) {
                            wordMap.put(word, wordMap.get(word).intValue() + 1);
                        } else {
                            wordMap.put(word, 1);
                        }
                    }
                }

                modelLine = bufferedModelReader.readLine();
                relationLine = bufferedRelationReader.readLine();
            }


            ValueComparator bvc =  new ValueComparator(wordMap);
            TreeMap<String, Integer> sortedWordMap = new TreeMap<String,Integer>(bvc);

            sortedWordMap.putAll(wordMap);

            return sortedWordMap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String createDate(File model) {
        String[] splitName = model.getName().split("-");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(splitName[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(splitName[1])-1);
        calendar.set(Calendar.DATE, Integer.valueOf(splitName[2]));

        return DATE_FORMAT.format(calendar.getTime());
    }

    static class ValueComparator implements Comparator<String> {
        /* Class by: http://stackoverflow.com/users/157196/user157196 */

        Map<String, Integer> base;
        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }

    static class ValueComparator2 implements Comparator<String> {
        /* Class by: http://stackoverflow.com/users/157196/user157196 */

        Map<String, Double> base;
        public ValueComparator2(Map<String, Double> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }
}

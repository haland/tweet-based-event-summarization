package com.chrishaland.tbes.builds.evaluation.macro;

import com.chrishaland.tbes.builds.evaluation.Evaluating;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;

public class SplitMacroEvaluation {
    private static final String[] CLASSIFIER_NAMES = {"rf", "nb", "id"};
    private static final Classifier[] CLASSIFIER_OBJECTS = {new RandomForest(), new NaiveBayes(), new J48()};

    private static final String[] AMBIGUOUS = {"suits", "vikings", "supernatural", "firefly"};
    private static final String[] UNAMBIGUOUS = {"agents_of_shield", "big_bang_theory_the", "doctor_who",
            "hannibal", "whose_line_is_it_anyway"};

    private static final String[] TOPIC_NAMES = {"Ambiguous", "Unambiguous"};
    private static final String[][] TOPIC_OBJECTS = {AMBIGUOUS, UNAMBIGUOUS};

    private static Calendar ct;

    public static void main(String[] args) {
        for (int i = 0; i < TOPIC_OBJECTS.length; i++) {
            for (int j = 0; j < CLASSIFIER_OBJECTS.length; j++) {
                File[] models = new File("data/weka/testing/model/").listFiles();
                ct = Evaluating.createDates(models);

                ArrayList<Double> precisionList = new ArrayList<Double>();

                for (int k = 0; k < models.length - 1; k++) {
                    Instances trainingModel = Evaluating.readInstances("data/weka/testing/model/" +
                            Evaluating.printDate(ct) + "-model.arff", "data/weka/testing/relations/" +
                            Evaluating.printDate(ct) + "-relations.arff", TOPIC_OBJECTS[i]);

                    double precisionTotal = 0.0;
                    for (int l = 0; l < TOPIC_OBJECTS[i].length; l++) {
                        Instances testData = Evaluating.readInstances("data/weka/complete/model.arff",
                                "data/weka/complete/relations.arff", TOPIC_OBJECTS[i][l]);
                        int instanceCount = Evaluating.instanceCountInModel("data/weka/testing/relations/" +
                                Evaluating.printDate(ct) + "-relations.arff", TOPIC_OBJECTS[i][l]);

                        for (int m = instanceCount - 1; m >= 0; m--) {
                            testData.delete(m);
                        }

                        precisionTotal += Evaluating.evaluate(CLASSIFIER_OBJECTS[j], trainingModel, testData);
                    }

                    double precision = precisionTotal / TOPIC_OBJECTS[i].length;
                    precisionList.add(precision);
                    ct.add(Calendar.DATE, 1);
                }

                System.out.print(CLASSIFIER_NAMES[j] + TOPIC_NAMES[i] + " = [");

                boolean isFirst = true;
                for (int k = 0; k < precisionList.size(); k++) {
                    if (precisionList.get(k) != 0 && !Double.isNaN(precisionList.get(k)) &&
                            !Double.isInfinite(precisionList.get(k))) {
                        if (isFirst) {
                            isFirst = false;
                            System.out.print(Evaluating.printDouble(precisionList.get(k)));
                        } else {
                            System.out.print(", " + Evaluating.printDouble(precisionList.get(k)));
                        }
                    }
                }

                System.out.println("];");
            }

            System.out.println();
        }
    }
}
package com.chrishaland.tbes.builds.evaluation.macro;

import com.chrishaland.tbes.builds.evaluation.Evaluating;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.*;
import weka.core.converters.ConverterUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;

public class SingleMacroEvaluation {
    private static final String[] CLASSIFIER_NAMES = { "rf", "nb", "id" };
    private static final Classifier[] CLASSIFIER_OBJECTS = { new RandomForest(), new NaiveBayes(), new J48()};

    private static final String[] TOPIC_OBJECTS = { "agents_of_shield", "big_bang_theory_the", "doctor_who",
            "firefly", "hannibal", "suits", "supernatural", "vikings", "whose_line_is_it_anyway" };

    private static Calendar ct;

    public static void main(String[] args) {
        for (int i = 0; i < CLASSIFIER_OBJECTS.length; i++) {
            File[] models = new File("data/weka/testing/model/").listFiles();
            ct = Evaluating.createDates(models);

            ArrayList<Double> precisionList = new ArrayList<Double>();
            for (int j = 0; j < models.length - 1; j++) {
                try {
                    ConverterUtils.DataSource trainingSource = new ConverterUtils.DataSource(
                            "data/weka/testing/model/" + Evaluating.printDate(ct) + "-model.arff");
                    Instances trainingModel = trainingSource.getDataSet();

                    if (trainingModel.classIndex() == -1) {
                        trainingModel.setClassIndex(trainingModel.numAttributes() - 1);
                    }

                    double precisionTotal = 0.0;
                    for (int k = 0; k < TOPIC_OBJECTS.length; k++) {
                        Instances testData = Evaluating.readInstances("data/weka/complete/model.arff",
                                "data/weka/complete/relations.arff", TOPIC_OBJECTS[k]);

                        int instanceCount = Evaluating.instanceCountInModel("data/weka/testing/relations/" +
                                Evaluating.printDate(ct) + "-relations.arff", TOPIC_OBJECTS[k]);

                        for (int l = instanceCount - 1; l >= 0; l--) {
                            testData.delete(l);
                        }

                        precisionTotal += Evaluating.evaluate(CLASSIFIER_OBJECTS[i], trainingModel, testData);
                    }

                    double precision = precisionTotal / TOPIC_OBJECTS.length;
                    precisionList.add(precision);

                    ct.add(Calendar.DATE, 1);
                } catch (Exception e) {

                }
            }

            System.out.print(CLASSIFIER_NAMES[i] + "Grouped = [");

            boolean isFirst = true;
            for (int j = 0; j < precisionList.size(); j++) {
                if (precisionList.get(j) != 0 && !Double.isNaN(precisionList.get(j)) &&
                        !Double.isInfinite(precisionList.get(j))) {
                    if (isFirst) {
                        isFirst = false;
                        System.out.print(Evaluating.printDouble(precisionList.get(j)));
                    } else {
                        System.out.print(", " + Evaluating.printDouble(precisionList.get(j)));
                    }
                }
            }

            System.out.println("];");
        }

        System.out.println();
    }
}

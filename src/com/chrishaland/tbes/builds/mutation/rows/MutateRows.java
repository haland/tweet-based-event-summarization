package com.chrishaland.tbes.builds.mutation.rows;

import com.chrishaland.tbes.model.hbase.HBaseConnection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.RowMutations;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class MutateRows {
    public static void main(String[] args) {
        try {
            String row = "vikings::8755596072912289791";
            HTable table = HBaseConnection.getTable("relation");

            Put put = new Put(Bytes.toBytes(row));
            put.add(Bytes.toBytes("feedback"), Bytes.toBytes("feedback_is_positive"), Bytes.toBytes("false"));

            RowMutations mutations = new RowMutations(Bytes.toBytes(row));
            mutations.add(put);

            table.mutateRow(mutations);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

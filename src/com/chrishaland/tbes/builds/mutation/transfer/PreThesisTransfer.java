package com.chrishaland.tbes.builds.mutation.transfer;

import com.chrishaland.tbes.controller.filtering.FilterHandler;
import com.chrishaland.tbes.controller.hbase.HBaseHandler;
import com.chrishaland.tbes.controller.topic.TopicHandler;
import com.chrishaland.tbes.model.data.Construct;
import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.publisher.Publisher;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.mysql.DatabaseConnection;
import com.chrishaland.tbes.model.utils.channel.Channel;
import com.chrishaland.tbes.view.SwingConsole;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * Transfers the document sample from the pre-thesis project from its MySQL server to HBase. The sample contained
 * 500 000 documents.
 */

public class PreThesisTransfer {

    public static void main(String[] args) {
        HBaseHandler.removeHBaseTables();
        HBaseHandler.createHBaseTables();

        TopicHandler topicHandler = new TopicHandler();
        TopicMap topicMap = topicHandler.getTopicMap();

        Channel<Document> documentChannel = new Channel<Document>(1000);
        Channel<Construct> constructChannel = new Channel<Construct>(1000);

        new FilterHandler(documentChannel, constructChannel, topicMap);
        new HBaseHandler(constructChannel);

        try {
            DatabaseConnection databaseConnection = new DatabaseConnection();

            String preTableName = databaseConnection.getPreTableFromConfiguration();
            String countQuery = "SELECT COUNT(*) FROM " + preTableName + "document";

            DatabaseConnection.Query countResultQuery = databaseConnection.executeQuery(countQuery);
            ResultSet countResultSet = countResultQuery.getResultSet();

            countResultSet.absolute(1);
            int entries = countResultSet.getInt(1);

            countResultQuery.close();

            int start = 0;
            int numberOfTweets = 1000;

            long count = 0;
            while (start < entries) {
                String tweetQuery = "SELECT * FROM " + preTableName + "document LIMIT " + String.valueOf(start) +
                        ", " + String.valueOf(numberOfTweets);

                DatabaseConnection.Query tweetResultQuery = databaseConnection.executeQuery(tweetQuery);
                ResultSet tweetResultSet = tweetResultQuery.getResultSet();

                while (tweetResultSet.next()) {
                    count++;

                    long id = tweetResultSet.getLong("id");
                    String user = tweetResultSet.getString("user");
                    String name = tweetResultSet.getString("name").replace("'", "\\'");
                    String text = tweetResultSet.getString("text").replace("'", "\\'");
                    Date timestamp = tweetResultSet.getDate("timestamp");

                    Publisher publisher = new Publisher(count, name, user, "unknown", 0, 0);
                    Document document = new Document(id, text, timestamp, 0, 0, publisher);

                    try {
                        documentChannel.put(document);
                    } catch (InterruptedException e) {
                        SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
                    }
                }

                start += numberOfTweets;
                tweetResultQuery.close();
            }
        } catch (SQLException e) {
            SwingConsole.getInstance().log(Level.SEVERE, e.getStackTrace());
        }
    }
}

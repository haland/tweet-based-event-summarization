package com.chrishaland.tbes.builds.mutation.documents;

import com.chrishaland.tbes.controller.topic.TopicHandler;
import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.publisher.Publisher;
import com.chrishaland.tbes.model.data.relation.Relation;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.filters.RegexFilter;
import com.chrishaland.tbes.model.hbase.tables.DocumentTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

public class SingleDayDocumentCount {
    private static final SimpleDateFormat DATE_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    }

    public static void main(String[] args) {
        TopicHandler topicHandler = new TopicHandler();
        TopicMap topicMap = topicHandler.getTopicMap();

        RegexFilter regexFilter = new RegexFilter(topicMap);

        Calendar calendar = getCalendar(4);

        SingleColumnValueFilter startAtFilter = new SingleColumnValueFilter(Bytes.toBytes("document"),
                Bytes.toBytes("created_at"), CompareFilter.CompareOp.GREATER,
                Bytes.toBytes(DATE_FORMAT.format(calendar.getTime())));

        calendar.add(Calendar.DATE, 1);

        SingleColumnValueFilter stopAtFilter = new SingleColumnValueFilter(Bytes.toBytes("document"),
                Bytes.toBytes("created_at"), CompareFilter.CompareOp.LESS,
                Bytes.toBytes(DATE_FORMAT.format(calendar.getTime())));

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(startAtFilter);
        filterList.addFilter(stopAtFilter);

        ResultScanner resultScanner = DocumentTable.filter(filterList);
        Iterator<Result> i = resultScanner.iterator();

        int documentCount = 0;
        int relationsCount = 0;
        int passedFilterCount = 0;
        while(i.hasNext()) {
            documentCount++;
            Result result = i.next();
            Relation[] relations = null;

            try {
                Document document = new Document(
                        Long.valueOf(new String(result.getRow())),
                        new String(result.getValue(Bytes.toBytes("document"), Bytes.toBytes("text"))),
                        DATE_FORMAT.parse(new String(result.getValue(Bytes.toBytes("document"), Bytes.toBytes("created_at")))),
                        Integer.valueOf(new String(result.getValue(Bytes.toBytes("document"), Bytes.toBytes("retweet_count")))),
                        Integer.valueOf(new String(result.getValue(Bytes.toBytes("document"), Bytes.toBytes("favorite_count")))),
                        new Publisher(0, "", "", "", 0, 0)
                );

                relations = regexFilter.filter(document);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (relations != null) {
                passedFilterCount++;
                relationsCount += relations.length;
            }
        }

        System.out.println("Documents found in HBase: " + documentCount);
        System.out.println("Documents passed filter 1: " + passedFilterCount);
        System.out.println("Relations created by filter 1: " + relationsCount);

    }

    private static Calendar getCalendar(int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.APRIL);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendar.set(Calendar.HOUR, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }
}

package com.chrishaland.tbes.builds.mutation.documents;

import com.chrishaland.tbes.model.data.relation.TrackerData;
import com.chrishaland.tbes.model.data.topic.Topic;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.hbase.tables.DocumentTable;
import com.chrishaland.tbes.model.hbase.tables.RelationTable;
import com.chrishaland.tbes.model.utils.file.Writer;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CompleteModel {
    private static final DateFormat DATE_FORMAT;

    private static TopicMap topicMap;
    private static Calendar calendar;
    private static Writer modelWriter, relationsWriter;
    private static ArrayList<String> idList, modelList, relationList;

    static {
        DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

        topicMap = new TopicMap();

        idList = new ArrayList<String>();
        modelList = new ArrayList<String>();
        relationList = new ArrayList<String>();
        modelWriter = new Writer("data/weka/complete/model.arff");
        relationsWriter = new Writer("data/weka/complete/relations.arff");

        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.MAY);
        calendar.set(Calendar.DATE, 15);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
    }

    public static void main(String[] args) {
        writeModelAttributes();
        writeRelationsAttributes();

        getAllFeedback(idList, modelList, relationList, topicMap);

        for (int i = 0; i < modelList.size(); i++) {
            modelWriter.writeLine(modelList.get(i));
            relationsWriter.writeLine(relationList.get(i));
        }

        modelWriter.close();
        relationsWriter.close();
    }

    private static void getAllFeedback(ArrayList<String> idList, ArrayList<String> modelList,
                                       ArrayList<String> relationList, TopicMap topicMap) {
        SingleColumnValueFilter hasFeedbackFilter = new SingleColumnValueFilter(Bytes.toBytes("feedback"),
                Bytes.toBytes("has_feedback"), CompareFilter.CompareOp.EQUAL, Bytes.toBytes("true"));

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(hasFeedbackFilter);

        ResultScanner resultScanner = RelationTable.filter(filterList);
        Iterator<Result> i = resultScanner.iterator();
        while(i.hasNext()) {
            Result relation = i.next();
            String topicId = new String(relation.getValue(Bytes.toBytes("relation"), Bytes.toBytes("topic_id")));
            String documentId = new String(relation.getValue(Bytes.toBytes("relation"), Bytes.toBytes("document_id")));

            boolean feedbackIsPositive = Boolean.valueOf(new String(relation.getValue(Bytes.toBytes("feedback"),
                    Bytes.toBytes("feedback_is_positive"))));

            Topic topic = topicMap.get(topicId);
            Result document = DocumentTable.get(documentId);
            String documentText = new String(document.getValue(Bytes.toBytes("document"), Bytes.toBytes("text")));

            TrackerData trackerData = getDocumentMatches(documentText, topic);

            String entry = String.valueOf(trackerData.getPositiveTrackers()) + ", " +
                    String.valueOf(trackerData.getPositiveTrackersAsHashtags()) + ", " +
                    String.valueOf(trackerData.getNegativeTrackers()) + ", " +
                    String.valueOf(trackerData.getChildrenTrackers()) + ", " +
                    (feedbackIsPositive ? "positive" : "negative");

            modelList.add(entry);
            idList.add(documentId);
            relationList.add(documentId + ", " + topicId);
        }
    }

    private static TrackerData getDocumentMatches(String documentText, Topic topic) {
        while(!topic.isRoot()) {
            topic = topic.getParent();
        }

        TrackerData trackerData = new TrackerData();

        Pattern positiveRegexPattern = Pattern.compile(topic.getRegex());
        Matcher positiveRegexMatcher = positiveRegexPattern.matcher(documentText);
        while(positiveRegexMatcher.find()) {
            trackerData.incrementPositiveTrackers();
        }

        Pattern positiveRegexAsHashtagPattern = Pattern.compile(topic.getRegex());
        Matcher positiveRegexAsHashtagMatcher = positiveRegexAsHashtagPattern.matcher(documentText);
        while(positiveRegexAsHashtagMatcher.find()) {
            trackerData.incrementPositiveTrackersAsHashtags();
        }

        Pattern positiveTrackersPattern = Pattern.compile(topic.getPositiveTrackersRegex());
        Matcher positiveTrackersMatcher = positiveTrackersPattern.matcher(documentText);
        while(positiveTrackersMatcher.find()) {
            trackerData.incrementPositiveTrackers();
        }

        Pattern positiveTrackersAsHashtagsPattern = Pattern.compile("#" + topic.getPositiveTrackersRegex());
        Matcher positiveTrackersAsHashtagsMatcher = positiveTrackersAsHashtagsPattern.matcher(documentText);
        while(positiveTrackersAsHashtagsMatcher.find()) {
            trackerData.incrementPositiveTrackersAsHashtags();
        }

        Pattern negativeTrackersPattern = Pattern.compile(topic.getNegativeTrackersRegex());
        Matcher negativeTrackersMatcher = negativeTrackersPattern.matcher(documentText);
        while(negativeTrackersMatcher.find()) {
            trackerData.incrementNegativeTrackers();
        }

        trackerData.setChildrenTrackers(countChildrenTrackers(topic, documentText));

        return trackerData;
    }

    private static int countChildrenTrackers(Topic topic, String documentText) {
        int level = 0;

        for (Topic child : topic.getChildren()) {
            Pattern childTrackerPattern = Pattern.compile(topic.getRegex());
            Matcher childTrackerMatcher = childTrackerPattern.matcher(documentText);

            int newLevel;
            if (childTrackerMatcher.find()) {
                newLevel = countChildrenTrackers(child, documentText);
            } else {
                newLevel = topic.getLevel() - 1;
            }

            if (newLevel > level) {
                level = newLevel;
            }
        }

        return level;
    }

    private static void writeModelAttributes() {
        modelWriter.writeLine("@relation document");
        modelWriter.writeLine("@attribute positive_trackers numeric");
        modelWriter.writeLine("@attribute positive_trackers_as_hashtags numeric");
        modelWriter.writeLine("@attribute negative_trackers numeric");
        modelWriter.writeLine("@attribute children_trackers numeric");
        modelWriter.writeLine("@attribute relation {positive, negative}");

        modelWriter.writeLine("");
        modelWriter.writeLine("@data");
    }

    private static void writeRelationsAttributes() {
        relationsWriter.writeLine("@relation relation");
        relationsWriter.writeLine("@attribute document_id numeric");
        relationsWriter.writeLine("@attribute topic_id nominal");

        relationsWriter.writeLine("");
        relationsWriter.writeLine("@data");
    }
}

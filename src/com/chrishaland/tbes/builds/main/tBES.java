package com.chrishaland.tbes.builds.main;

import com.chrishaland.tbes.controller.filtering.FilterHandler;
import com.chrishaland.tbes.controller.hbase.HBaseHandler;
import com.chrishaland.tbes.controller.topic.TopicHandler;
import com.chrishaland.tbes.controller.twitter.TweetHandler;
import com.chrishaland.tbes.model.data.Construct;
import com.chrishaland.tbes.model.data.document.Document;
import com.chrishaland.tbes.model.data.topic.tree.TopicMap;
import com.chrishaland.tbes.model.utils.channel.Channel;
import com.chrishaland.tbes.view.SwingConsole;
import com.chrishaland.tbes.view.listeners.CommandEvent;
import com.chrishaland.tbes.view.listeners.CommandListener;

import java.util.logging.Level;

public class tBES implements CommandListener {
    private TweetHandler tweetHandler;

    public tBES() {
        SwingConsole.getInstance().addCommandListener(this);

        HBaseHandler.createHBaseTables();

        TopicHandler topicHandler = new TopicHandler();
        TopicMap topicMap = topicHandler.getTopicMap();

        Channel<Document> documentChannel = new Channel<Document>(1000);
        Channel<Construct> constructChannel = new Channel<Construct>(1000);

        new FilterHandler(documentChannel, constructChannel, topicMap);
        new HBaseHandler(constructChannel);

        tweetHandler = new TweetHandler(documentChannel);
    }

    public static void main(String[] args) {
        if (argExists("--deleteExistingTables", args)) {
            TopicHandler.dropTopicTable();
            HBaseHandler.removeHBaseTables();
        }

        new tBES();
    }

    public static boolean argExists(String s, String args[]) {
        boolean argFound = false;

        for(String arg : args) {
            if (arg.equals(s)) {
                argFound = true;
                break;
            }
        }

        return argFound;
    }

    @Override
    public void onCommandReceived(CommandEvent e) {
        switch (e.getCommand()) {
            case CommandEvent.EXIT:
                SwingConsole.getInstance().log(Level.INFO, "The system is shutting down.");

                if (tweetHandler != null) {
                    tweetHandler.getTwitterConnection().stop();
                }

                System.exit(0);
                break;
            default:
                break;
        }
    }
}
